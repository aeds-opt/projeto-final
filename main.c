#define NCURSES_MOUSE_VERSION
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "includes/curses.h"

#include "includes/GUI.h"
#include "includes/sigaa.h"

#define White 1
#define Blue 2
#define Green 3
#define Red 4
#define Yellow 5
#define Black 6
#define Success 7
#define Danger 8

#define Escape 27

#define LoginField 0
#define PasswordField 1
#define LoginButton 2
#define SignInButton 3

#define LoginFieldSignIn 0
#define PasswordFieldSignIn 1
#define NameFieldSignIn 2
#define VoltarButtonSignIn 3
#define SignInButtonSignIn 4

#define MateriaOnTopPosition 0
#define MateriaOnMiddlePosition 1
#define MateriaOnBottomPosition 2
#define NextPageMateriaButtonMainMenu 3
#define PreviousPageMateriaButtonMainMenu 4
#define AtividadeOnTopPosition 5
#define AtividadeOnMiddlePosition 6
#define AtividadeOnBottomPosition 7
#define NextPageAtividadeButtonMainMenu 8
#define PreviousPageAtividadeButtonMainMenu 9
#define CadastrarMateriaButtonMainMenu 10
#define CadastrarAtividadeButtonMainMenu 11
#define EditarSenhaButtonMainMenu 12
#define LogoutButtonMainMenu 13

#define NameFieldCadastrarMateria 0
#define VoltarButtonCadastrarMateria 1
#define ButtonCadastrarMateria 2

#define NameFieldCadastrarAtividade 0
#define DescricaoFieldCadastrarAtividade 1
#define ValorFieldCadastrarAtividade 2
#define DataFieldCadastrarAtividade 3
#define NextPageMateriaButtonCadastrarAtividade 4
#define PreviousPageMateriaButtonCadastrarAtividade 5
#define VoltarButtonCadastrarAtividade 6
#define ButtonCadastrarAtividade 7

#define VoltarButtonListarTarefas 0
#define NextPageAtividadeListarTarefas 2
#define PreviousPageAtividadeListarTarefas 1

#define Text 0
#define Password 1
#define Number 2
#define Data 3

#define VoltarButtonDetalhesMateria 0
#define ButtonTrancarMateria 1
#define ButtonListarAtividadesDetalhesMateria 2
#define ButtonEditarMateria 3

#define VoltarButtonDetalhesAtividade 0
#define ButtonRemoverAtividade 1
#define ButtonEntregarAtividade 2
#define ButtonEditarAtividade 3

#define MotivoAtraso -1
#define MotivoEntregue 0
#define Sucesso 1

#define Nome 0
#define Descricao 1
#define Valor 2
#define Data 3

#define NameFieldEditarAtividade 0
#define DescricaoFieldEditarAtividade 1
#define ValorFieldEditarAtividade 2
#define DataFieldEditarAtividade 3
#define VoltarButtonEditarAtividade 4
#define ButtonAtualizarAtividade 5

#define PasswordFieldChangePassword 0
#define VoltarButtonChangePassword  1
#define ButtonChangePassword 2

int main()
{
    int ch = 0;
    CoordinatePairs **loginBoxes;
	MEVENT event;
    mmask_t old;

    initCursor();
    mousemask (ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, &old);

    char *userLogin = calloc(1,sizeof(char)), *userPassword = calloc(1,sizeof(char));

    loginBoxes = showLoginScreen();

    while(TRUE)
    {
        ch = getch();
        if(ch == Escape){
            break;
        }
        else if(ch == KEY_MOUSE) {
            if(getmouse(&event) == OK){
                if(event.bstate & BUTTON1_CLICKED) {
                    if(validClick(loginBoxes[LoginField], event)){
                        userLogin = setField(loginBoxes[LoginField], Text, 12, userLogin);
                    }
                    else if(validClick(loginBoxes[PasswordField], event)){
                        userPassword = setField(loginBoxes[PasswordField], Password, 8, userPassword);
                    }
                    else if(validClick(loginBoxes[LoginButton], event)){
                        int validLogin;
                        validLogin = login(userLogin, userPassword, loginBoxes[LoginField], loginBoxes[PasswordField]);
                        if(validLogin == TRUE){ //TROCAR AQUI PARA HABILITYAR O LOGIN
                            userLogin[0] = '\0';
                            userPassword[0] = '\0';
                            halfdelay(10);
                            getch();
                            nocbreak();
                            int paginacaoMaterias = 1, paginacaoTarefas = 1;
                            CoordinatePairs **mainMenuBoxes;
                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);

                            while(TRUE){
                                ch = getch();
                                if(ch == KEY_MOUSE) {
                                    if(getmouse(&event) == OK){
                                        if(event.bstate & BUTTON1_CLICKED) {
                                            if(validClick(mainMenuBoxes[MateriaOnTopPosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesMateriaBoxes;
                                                int indexMateria;

                                                indexMateria = (paginacaoMaterias * 3) - 3;

                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);

                                                int canLoad = TRUE;

                                                if((detalhesMateriaBoxes[0]->column == 0)&&(detalhesMateriaBoxes[0]->line == 0)){
                                                    showErrorMessage(2);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesMateriaBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesMateriaBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesMateriaBoxes[VoltarButtonDetalhesMateria], event)){
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonTrancarMateria], event)){
                                                                        trancarMateria(indexMateria);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonListarAtividadesDetalhesMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **listarAtividadeBoxes;

                                                                        int pagionacaoListarAtividade =1;


                                                                        listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((listarAtividadeBoxes[0]->column == 0)&&(listarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(3);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(listarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){

                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(listarAtividadeBoxes);
                                                                                    detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(listarAtividadeBoxes[NextPageAtividadeListarTarefas], event)){
                                                                                                int sizeArrayAtividades = getAtividadesDaMateriaArraySize();
                                                                                                if(pagionacaoListarAtividade * 3 < sizeArrayAtividades){
                                                                                                    pagionacaoListarAtividade++;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[PreviousPageAtividadeListarTarefas], event)){
                                                                                                if(pagionacaoListarAtividade > 1){
                                                                                                    pagionacaoListarAtividade--;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[VoltarButtonListarTarefas], event)){
                                                                                                free(listarAtividadeBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonEditarMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarMateriaBoxes;
                                                                        char *nameEditarMateria = calloc(1,sizeof(char));
                                                                        nameEditarMateria = getMateriaName(indexMateria);
                                                                        editarMateriaBoxes = showEditarMateriaScreen(indexMateria);
                                                                        while(TRUE){
                                                                            ch = getch();
                                                                            if(ch == Escape){
                                                                                free(editarMateriaBoxes);
                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                break;
                                                                            }

                                                                            if(ch == KEY_MOUSE) {
                                                                                if(getmouse(&event) == OK){
                                                                                    if(event.bstate & BUTTON1_CLICKED) {
                                                                                        if(validClick(editarMateriaBoxes[NameFieldCadastrarMateria], event)){
                                                                                            nameEditarMateria = setField(editarMateriaBoxes[NameFieldCadastrarMateria], Text, 30, nameEditarMateria);
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[VoltarButtonCadastrarMateria], event)){
                                                                                            free(editarMateriaBoxes);
                                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                            break;
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[ButtonCadastrarMateria], event)){
                                                                                            int validMateria;
                                                                                            validMateria = editarMateria(indexMateria, nameEditarMateria, editarMateriaBoxes[NameFieldCadastrarMateria]);
                                                                                            if(validMateria == TRUE){
                                                                                                halfdelay(10);
                                                                                                getch();
                                                                                                nocbreak();
                                                                                                free(editarMateriaBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[MateriaOnMiddlePosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesMateriaBoxes;
                                                int indexMateria;

                                                indexMateria = (paginacaoMaterias * 3) - 2;

                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);

                                                int canLoad = TRUE;

                                                if((detalhesMateriaBoxes[0]->column == 0)&&(detalhesMateriaBoxes[0]->line == 0)){
                                                    showErrorMessage(2);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesMateriaBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesMateriaBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesMateriaBoxes[VoltarButtonDetalhesMateria], event)){
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonTrancarMateria], event)){
                                                                        trancarMateria(indexMateria);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonListarAtividadesDetalhesMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **listarAtividadeBoxes;

                                                                        int pagionacaoListarAtividade =1;


                                                                        listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((listarAtividadeBoxes[0]->column == 0)&&(listarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(3);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(listarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){

                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(listarAtividadeBoxes);
                                                                                    detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(listarAtividadeBoxes[NextPageAtividadeListarTarefas], event)){
                                                                                                int sizeArrayAtividades = getAtividadesDaMateriaArraySize();
                                                                                                if(pagionacaoListarAtividade * 3 < sizeArrayAtividades){
                                                                                                    pagionacaoListarAtividade++;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[PreviousPageAtividadeListarTarefas], event)){
                                                                                                if(pagionacaoListarAtividade > 1){
                                                                                                    pagionacaoListarAtividade--;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[VoltarButtonListarTarefas], event)){
                                                                                                free(listarAtividadeBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonEditarMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarMateriaBoxes;
                                                                        char *nameEditarMateria = calloc(1,sizeof(char));
                                                                        nameEditarMateria = getMateriaName(indexMateria);
                                                                        editarMateriaBoxes = showEditarMateriaScreen(indexMateria);
                                                                        while(TRUE){
                                                                            ch = getch();
                                                                            if(ch == Escape){
                                                                                free(editarMateriaBoxes);
                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                break;
                                                                            }

                                                                            if(ch == KEY_MOUSE) {
                                                                                if(getmouse(&event) == OK){
                                                                                    if(event.bstate & BUTTON1_CLICKED) {
                                                                                        if(validClick(editarMateriaBoxes[NameFieldCadastrarMateria], event)){
                                                                                            nameEditarMateria = setField(editarMateriaBoxes[NameFieldCadastrarMateria], Text, 30, nameEditarMateria);
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[VoltarButtonCadastrarMateria], event)){
                                                                                            free(editarMateriaBoxes);
                                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                            break;
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[ButtonCadastrarMateria], event)){
                                                                                            int validMateria;
                                                                                            validMateria = editarMateria(indexMateria, nameEditarMateria, editarMateriaBoxes[NameFieldCadastrarMateria]);
                                                                                            if(validMateria == TRUE){
                                                                                                halfdelay(10);
                                                                                                getch();
                                                                                                nocbreak();
                                                                                                free(editarMateriaBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[MateriaOnBottomPosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesMateriaBoxes;
                                                int indexMateria;

                                                indexMateria = (paginacaoMaterias * 3) - 1;

                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);

                                                int canLoad = TRUE;

                                                if((detalhesMateriaBoxes[0]->column == 0)&&(detalhesMateriaBoxes[0]->line == 0)){
                                                    showErrorMessage(2);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesMateriaBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesMateriaBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesMateriaBoxes[VoltarButtonDetalhesMateria], event)){
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonTrancarMateria], event)){
                                                                        trancarMateria(indexMateria);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonListarAtividadesDetalhesMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **listarAtividadeBoxes;

                                                                        int pagionacaoListarAtividade =1;


                                                                        listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((listarAtividadeBoxes[0]->column == 0)&&(listarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(3);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(listarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){

                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(listarAtividadeBoxes);
                                                                                    detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(listarAtividadeBoxes[NextPageAtividadeListarTarefas], event)){
                                                                                                int sizeArrayAtividades = getAtividadesDaMateriaArraySize();
                                                                                                if(pagionacaoListarAtividade * 3 < sizeArrayAtividades){
                                                                                                    pagionacaoListarAtividade++;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[PreviousPageAtividadeListarTarefas], event)){
                                                                                                if(pagionacaoListarAtividade > 1){
                                                                                                    pagionacaoListarAtividade--;
                                                                                                    listarAtividadeBoxes = showListarAtividadesScreen(indexMateria, pagionacaoListarAtividade);
                                                                                                }
                                                                                            }
                                                                                            else if(validClick(listarAtividadeBoxes[VoltarButtonListarTarefas], event)){
                                                                                                free(listarAtividadeBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesMateriaBoxes[ButtonEditarMateria], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarMateriaBoxes;
                                                                        char *nameEditarMateria = calloc(1,sizeof(char));
                                                                        nameEditarMateria = getMateriaName(indexMateria);
                                                                        editarMateriaBoxes = showEditarMateriaScreen(indexMateria);
                                                                        while(TRUE){
                                                                            ch = getch();
                                                                            if(ch == Escape){
                                                                                free(editarMateriaBoxes);
                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                break;
                                                                            }

                                                                            if(ch == KEY_MOUSE) {
                                                                                if(getmouse(&event) == OK){
                                                                                    if(event.bstate & BUTTON1_CLICKED) {
                                                                                        if(validClick(editarMateriaBoxes[NameFieldCadastrarMateria], event)){
                                                                                            nameEditarMateria = setField(editarMateriaBoxes[NameFieldCadastrarMateria], Text, 30, nameEditarMateria);
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[VoltarButtonCadastrarMateria], event)){
                                                                                            free(editarMateriaBoxes);
                                                                                            detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                            break;
                                                                                        }
                                                                                        else if(validClick(editarMateriaBoxes[ButtonCadastrarMateria], event)){
                                                                                            int validMateria;
                                                                                            validMateria = editarMateria(indexMateria, nameEditarMateria, editarMateriaBoxes[NameFieldCadastrarMateria]);
                                                                                            if(validMateria == TRUE){
                                                                                                halfdelay(10);
                                                                                                getch();
                                                                                                nocbreak();
                                                                                                free(editarMateriaBoxes);
                                                                                                detalhesMateriaBoxes = showDetalhesMateriaScreen(indexMateria);
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[NextPageMateriaButtonMainMenu], event)){
                                                int sizeArrayMaterias = getMateriasArraySize();
                                                if(paginacaoMaterias * 3 < sizeArrayMaterias){
                                                    paginacaoMaterias++;
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[PreviousPageMateriaButtonMainMenu], event)){
                                                if(paginacaoMaterias > 1){
                                                    paginacaoMaterias--;
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[AtividadeOnTopPosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesAtividadeBoxes;
                                                int indexAtividade;

                                                indexAtividade = (paginacaoTarefas * 3) - 3;

                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);

                                                int canLoad = TRUE;

                                                if((detalhesAtividadeBoxes[0]->column == 0)&&(detalhesAtividadeBoxes[0]->line == 0)){
                                                    showErrorMessage(4);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if((detalhesAtividadeBoxes[0]->column == 1)&&(detalhesAtividadeBoxes[0]->line == 1)){
                                                    showErrorMessage(5);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    removerAtividade(indexAtividade, TRUE);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesAtividadeBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesAtividadeBoxes[VoltarButtonDetalhesAtividade], event)){
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonRemoverAtividade], event)){
                                                                        removerAtividade(indexAtividade, FALSE);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEntregarAtividade], event)){
                                                                        int response;
                                                                        response = entregarAtividade(indexAtividade);
                                                                        if(response == MotivoAtraso){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == MotivoEntregue){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == Sucesso){
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEditarAtividade], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarAtividadeBoxes;
                                                                        char *nameEditarAtividade = calloc(1,sizeof(char));
                                                                        char *descricaoEditarAtividade = calloc(1,sizeof(char));
                                                                        char *valorEditarAtividade = calloc(1,sizeof(char));
                                                                        char *dataEditarAtividade = calloc(1,sizeof(char));

                                                                        nameEditarAtividade = getAtividadeProperty(indexAtividade, Nome);
                                                                        descricaoEditarAtividade = getAtividadeProperty(indexAtividade, Descricao);
                                                                        valorEditarAtividade = getAtividadeProperty(indexAtividade, Valor);
                                                                        dataEditarAtividade = getAtividadeProperty(indexAtividade, Data);

                                                                        editarAtividadeBoxes = showEditarAtividadeScreen(indexAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((editarAtividadeBoxes[0]->column == 0)&&(editarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        else if((editarAtividadeBoxes[0]->column == 1)&&(editarAtividadeBoxes[0]->line == 1)){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){
                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(editarAtividadeBoxes);
                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(editarAtividadeBoxes[NameFieldEditarAtividade], event)){
                                                                                                nameEditarAtividade = setField(editarAtividadeBoxes[NameFieldEditarAtividade], Text, 30, nameEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DescricaoFieldEditarAtividade], event)){
                                                                                                descricaoEditarAtividade = setField(editarAtividadeBoxes[DescricaoFieldEditarAtividade], Text, 38, descricaoEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ValorFieldEditarAtividade], event)){
                                                                                                valorEditarAtividade = setField(editarAtividadeBoxes[ValorFieldEditarAtividade], Number, 6, valorEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DataFieldEditarAtividade], event)){
                                                                                                dataEditarAtividade = setField(editarAtividadeBoxes[DataFieldEditarAtividade], Data, 10, dataEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[VoltarButtonEditarAtividade], event)){
                                                                                                free(editarAtividadeBoxes);
                                                                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                break;
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ButtonAtualizarAtividade], event)){
                                                                                                int validAtividade;
                                                                                                validAtividade = editarAtividade(indexAtividade, nameEditarAtividade, descricaoEditarAtividade, valorEditarAtividade, dataEditarAtividade, editarAtividadeBoxes[NameFieldEditarAtividade], editarAtividadeBoxes[DescricaoFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade]);
                                                                                                if(validAtividade == TRUE){
                                                                                                    halfdelay(10);
                                                                                                    getch();
                                                                                                    nocbreak();
                                                                                                    free(editarAtividadeBoxes);
                                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                    break;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[AtividadeOnMiddlePosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesAtividadeBoxes;
                                                int indexAtividade;

                                                indexAtividade = (paginacaoTarefas * 3) - 2;

                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);

                                                int canLoad = TRUE;

                                                if((detalhesAtividadeBoxes[0]->column == 0)&&(detalhesAtividadeBoxes[0]->line == 0)){
                                                    showErrorMessage(4);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if((detalhesAtividadeBoxes[0]->column == 1)&&(detalhesAtividadeBoxes[0]->line == 1)){
                                                    showErrorMessage(5);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    removerAtividade(indexAtividade, TRUE);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesAtividadeBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesAtividadeBoxes[VoltarButtonDetalhesAtividade], event)){
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonRemoverAtividade], event)){
                                                                        removerAtividade(indexAtividade, FALSE);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEntregarAtividade], event)){
                                                                        int response;
                                                                        response = entregarAtividade(indexAtividade);
                                                                        if(response == MotivoAtraso){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == MotivoEntregue){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == Sucesso){
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEditarAtividade], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarAtividadeBoxes;
                                                                        char *nameEditarAtividade = calloc(1,sizeof(char));
                                                                        char *descricaoEditarAtividade = calloc(1,sizeof(char));
                                                                        char *valorEditarAtividade = calloc(1,sizeof(char));
                                                                        char *dataEditarAtividade = calloc(1,sizeof(char));

                                                                        nameEditarAtividade = getAtividadeProperty(indexAtividade, Nome);
                                                                        descricaoEditarAtividade = getAtividadeProperty(indexAtividade, Descricao);
                                                                        valorEditarAtividade = getAtividadeProperty(indexAtividade, Valor);
                                                                        dataEditarAtividade = getAtividadeProperty(indexAtividade, Data);

                                                                        editarAtividadeBoxes = showEditarAtividadeScreen(indexAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((editarAtividadeBoxes[0]->column == 0)&&(editarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        else if((editarAtividadeBoxes[0]->column == 1)&&(editarAtividadeBoxes[0]->line == 1)){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){
                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(editarAtividadeBoxes);
                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(editarAtividadeBoxes[NameFieldEditarAtividade], event)){
                                                                                                nameEditarAtividade = setField(editarAtividadeBoxes[NameFieldEditarAtividade], Text, 30, nameEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DescricaoFieldEditarAtividade], event)){
                                                                                                descricaoEditarAtividade = setField(editarAtividadeBoxes[DescricaoFieldEditarAtividade], Text, 38, descricaoEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ValorFieldEditarAtividade], event)){
                                                                                                valorEditarAtividade = setField(editarAtividadeBoxes[ValorFieldEditarAtividade], Number, 6, valorEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DataFieldEditarAtividade], event)){
                                                                                                dataEditarAtividade = setField(editarAtividadeBoxes[DataFieldEditarAtividade], Data, 10, dataEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[VoltarButtonEditarAtividade], event)){
                                                                                                free(editarAtividadeBoxes);
                                                                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                break;
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ButtonAtualizarAtividade], event)){
                                                                                                int validAtividade;
                                                                                                validAtividade = editarAtividade(indexAtividade, nameEditarAtividade, descricaoEditarAtividade, valorEditarAtividade, dataEditarAtividade, editarAtividadeBoxes[NameFieldEditarAtividade], editarAtividadeBoxes[DescricaoFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade]);
                                                                                                if(validAtividade == TRUE){
                                                                                                    halfdelay(10);
                                                                                                    getch();
                                                                                                    nocbreak();
                                                                                                    free(editarAtividadeBoxes);
                                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                    break;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[AtividadeOnBottomPosition], event)){
                                                clear();
                                                CoordinatePairs **detalhesAtividadeBoxes;
                                                int indexAtividade;

                                                indexAtividade = (paginacaoTarefas * 3) - 1;

                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);

                                                int canLoad = TRUE;

                                                if((detalhesAtividadeBoxes[0]->column == 0)&&(detalhesAtividadeBoxes[0]->line == 0)){
                                                    showErrorMessage(4);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if((detalhesAtividadeBoxes[0]->column == 1)&&(detalhesAtividadeBoxes[0]->line == 1)){
                                                    showErrorMessage(5);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    removerAtividade(indexAtividade, TRUE);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    free(detalhesAtividadeBoxes);
                                                    canLoad = FALSE;
                                                }
                                                if(canLoad == TRUE){
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(detalhesAtividadeBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(detalhesAtividadeBoxes[VoltarButtonDetalhesAtividade], event)){
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonRemoverAtividade], event)){
                                                                        removerAtividade(indexAtividade, FALSE);
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(detalhesAtividadeBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEntregarAtividade], event)){
                                                                        int response;
                                                                        response = entregarAtividade(indexAtividade);
                                                                        if(response == MotivoAtraso){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == MotivoEntregue){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                        else if(response == Sucesso){
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(detalhesAtividadeBoxes);
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                    else if(validClick(detalhesAtividadeBoxes[ButtonEditarAtividade], event)){
                                                                        clear();
                                                                        CoordinatePairs **editarAtividadeBoxes;
                                                                        char *nameEditarAtividade = calloc(1,sizeof(char));
                                                                        char *descricaoEditarAtividade = calloc(1,sizeof(char));
                                                                        char *valorEditarAtividade = calloc(1,sizeof(char));
                                                                        char *dataEditarAtividade = calloc(1,sizeof(char));

                                                                        nameEditarAtividade = getAtividadeProperty(indexAtividade, Nome);
                                                                        descricaoEditarAtividade = getAtividadeProperty(indexAtividade, Descricao);
                                                                        valorEditarAtividade = getAtividadeProperty(indexAtividade, Valor);
                                                                        dataEditarAtividade = getAtividadeProperty(indexAtividade, Data);

                                                                        editarAtividadeBoxes = showEditarAtividadeScreen(indexAtividade);

                                                                        int canLoad = TRUE;

                                                                        if((editarAtividadeBoxes[0]->column == 0)&&(editarAtividadeBoxes[0]->line == 0)){
                                                                            showErrorMessage(6);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        else if((editarAtividadeBoxes[0]->column == 1)&&(editarAtividadeBoxes[0]->line == 1)){
                                                                            showErrorMessage(7);
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(editarAtividadeBoxes);
                                                                            canLoad = FALSE;
                                                                        }
                                                                        if(canLoad == TRUE){
                                                                            while(TRUE){
                                                                                ch = getch();
                                                                                if(ch == Escape){
                                                                                    free(editarAtividadeBoxes);
                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                    break;
                                                                                }

                                                                                if(ch == KEY_MOUSE) {
                                                                                    if(getmouse(&event) == OK){
                                                                                        if(event.bstate & BUTTON1_CLICKED) {
                                                                                            if(validClick(editarAtividadeBoxes[NameFieldEditarAtividade], event)){
                                                                                                nameEditarAtividade = setField(editarAtividadeBoxes[NameFieldEditarAtividade], Text, 30, nameEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DescricaoFieldEditarAtividade], event)){
                                                                                                descricaoEditarAtividade = setField(editarAtividadeBoxes[DescricaoFieldEditarAtividade], Text, 38, descricaoEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ValorFieldEditarAtividade], event)){
                                                                                                valorEditarAtividade = setField(editarAtividadeBoxes[ValorFieldEditarAtividade], Number, 6, valorEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[DataFieldEditarAtividade], event)){
                                                                                                dataEditarAtividade = setField(editarAtividadeBoxes[DataFieldEditarAtividade], Data, 10, dataEditarAtividade);
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[VoltarButtonEditarAtividade], event)){
                                                                                                free(editarAtividadeBoxes);
                                                                                                detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                break;
                                                                                            }
                                                                                            else if(validClick(editarAtividadeBoxes[ButtonAtualizarAtividade], event)){
                                                                                                int validAtividade;
                                                                                                validAtividade = editarAtividade(indexAtividade, nameEditarAtividade, descricaoEditarAtividade, valorEditarAtividade, dataEditarAtividade, editarAtividadeBoxes[NameFieldEditarAtividade], editarAtividadeBoxes[DescricaoFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade], editarAtividadeBoxes[ValorFieldEditarAtividade]);
                                                                                                if(validAtividade == TRUE){
                                                                                                    halfdelay(10);
                                                                                                    getch();
                                                                                                    nocbreak();
                                                                                                    free(editarAtividadeBoxes);
                                                                                                    detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                                                    break;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else{
                                                                            detalhesAtividadeBoxes = showDetalhesAtividadeScreen(indexAtividade);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[NextPageAtividadeButtonMainMenu], event)){
                                                int sizeArrayAtividades = getAtividadesArraySize();
                                                if(paginacaoTarefas * 3 < sizeArrayAtividades){
                                                    paginacaoTarefas++;
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[PreviousPageAtividadeButtonMainMenu], event)){
                                                if(paginacaoTarefas > 1){
                                                    paginacaoTarefas--;
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[CadastrarMateriaButtonMainMenu], event)){
                                                clear();
                                                CoordinatePairs **cadastrarMateriaBoxes;
                                                char *nameCadastrarMateria = calloc(1,sizeof(char));
                                                cadastrarMateriaBoxes = showCadastrarMateriaScreen();
                                                while(TRUE){
                                                    ch = getch();
                                                    if(ch == Escape){
                                                        free(cadastrarMateriaBoxes);
                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                        break;
                                                    }

                                                    if(ch == KEY_MOUSE) {
                                                        if(getmouse(&event) == OK){
                                                            if(event.bstate & BUTTON1_CLICKED) {
                                                                if(validClick(cadastrarMateriaBoxes[NameFieldCadastrarMateria], event)){
                                                                    nameCadastrarMateria = setField(cadastrarMateriaBoxes[NameFieldCadastrarMateria], Text, 30, nameCadastrarMateria);
                                                                }
                                                                else if(validClick(cadastrarMateriaBoxes[VoltarButtonCadastrarMateria], event)){
                                                                    free(cadastrarMateriaBoxes);
                                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                    break;
                                                                }
                                                                else if(validClick(cadastrarMateriaBoxes[ButtonCadastrarMateria], event)){
                                                                    int validMateria;
                                                                    validMateria = cadastrarMateria(nameCadastrarMateria, cadastrarMateriaBoxes[NameFieldCadastrarMateria]);
                                                                    if(validMateria == TRUE){
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(cadastrarMateriaBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[CadastrarAtividadeButtonMainMenu], event)){
                                                clear();
                                                CoordinatePairs **cadastrarAtividadeBoxes;
                                                char *nameCadastrarAtividade = calloc(1,sizeof(char));
                                                char *descricaoCadastrarAtividade = calloc(1,sizeof(char));
                                                char *valorCadastrarAtividade = calloc(1,sizeof(char));
                                                char *dataCadastrarAtividade = calloc(1,sizeof(char));
                                                int indexMateria = 0;
                                                int canLoad = TRUE;

                                                if(getMateriasArraySize() == 0){
                                                    showErrorMessage(1);
                                                    halfdelay(10);
                                                    getch();
                                                    nocbreak();
                                                    canLoad = FALSE;
                                                }

                                                if(canLoad == TRUE){
                                                    cadastrarAtividadeBoxes = showCadastrarAtividadeScreen(indexMateria);
                                                    while(TRUE){
                                                        ch = getch();
                                                        if(ch == Escape){
                                                            free(cadastrarAtividadeBoxes);
                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                            break;
                                                        }

                                                        if(ch == KEY_MOUSE) {
                                                            if(getmouse(&event) == OK){
                                                                if(event.bstate & BUTTON1_CLICKED) {
                                                                    if(validClick(cadastrarAtividadeBoxes[NameFieldCadastrarAtividade], event)){
                                                                        nameCadastrarAtividade = setField(cadastrarAtividadeBoxes[NameFieldCadastrarAtividade], Text, 30, nameCadastrarAtividade);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[DescricaoFieldCadastrarAtividade], event)){
                                                                        descricaoCadastrarAtividade = setField(cadastrarAtividadeBoxes[DescricaoFieldCadastrarAtividade], Text, 38, descricaoCadastrarAtividade);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[ValorFieldCadastrarAtividade], event)){
                                                                        valorCadastrarAtividade = setField(cadastrarAtividadeBoxes[ValorFieldCadastrarAtividade], Number, 6, valorCadastrarAtividade);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[DataFieldCadastrarAtividade], event)){
                                                                        dataCadastrarAtividade = setField(cadastrarAtividadeBoxes[DataFieldCadastrarAtividade], Data, 10, dataCadastrarAtividade);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[NextPageMateriaButtonCadastrarAtividade], event)){
                                                                        int sizeArrayMaterias = getMateriasArraySize();
                                                                        if(indexMateria < sizeArrayMaterias - 1){
                                                                            indexMateria++;
                                                                        }
                                                                        cadastrarAtividadeBoxes = showCadastrarAtividadeScreen(indexMateria);
                                                                        autoSetField(nameCadastrarAtividade, cadastrarAtividadeBoxes[NameFieldCadastrarAtividade]);
                                                                        autoSetField(descricaoCadastrarAtividade, cadastrarAtividadeBoxes[DescricaoFieldCadastrarAtividade]);
                                                                        autoSetField(valorCadastrarAtividade, cadastrarAtividadeBoxes[ValorFieldCadastrarAtividade]);
                                                                        autoSetField(dataCadastrarAtividade, cadastrarAtividadeBoxes[DataFieldCadastrarAtividade]);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[PreviousPageMateriaButtonCadastrarAtividade], event)){
                                                                        if(indexMateria > 0){
                                                                            indexMateria--;
                                                                        }
                                                                        cadastrarAtividadeBoxes = showCadastrarAtividadeScreen(indexMateria);
                                                                        autoSetField(nameCadastrarAtividade, cadastrarAtividadeBoxes[NameFieldCadastrarAtividade]);
                                                                        autoSetField(descricaoCadastrarAtividade, cadastrarAtividadeBoxes[DescricaoFieldCadastrarAtividade]);
                                                                        autoSetField(valorCadastrarAtividade, cadastrarAtividadeBoxes[ValorFieldCadastrarAtividade]);
                                                                        autoSetField(dataCadastrarAtividade, cadastrarAtividadeBoxes[DataFieldCadastrarAtividade]);
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[VoltarButtonCadastrarAtividade], event)){
                                                                        free(cadastrarAtividadeBoxes);
                                                                        cadastrarAtividadeBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                    else if(validClick(cadastrarAtividadeBoxes[ButtonCadastrarAtividade], event)){
                                                                        int validAtividade;
                                                                        validAtividade = cadastrarAtividade(nameCadastrarAtividade,descricaoCadastrarAtividade,valorCadastrarAtividade,dataCadastrarAtividade, indexMateria, cadastrarAtividadeBoxes[NameFieldCadastrarMateria], cadastrarAtividadeBoxes[DescricaoFieldCadastrarAtividade], cadastrarAtividadeBoxes[ValorFieldCadastrarAtividade], cadastrarAtividadeBoxes[DataFieldCadastrarAtividade]);
                                                                        if(validAtividade == TRUE){
                                                                            halfdelay(10);
                                                                            getch();
                                                                            nocbreak();
                                                                            free(cadastrarAtividadeBoxes);
                                                                            mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else{
                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[EditarSenhaButtonMainMenu], event)){
                                                clear();
                                                CoordinatePairs **changePasswordBoxes;
                                                char *userPassword = calloc(1,sizeof(char));
                                                changePasswordBoxes = showChangePasswordScreen();
                                                while(TRUE){
                                                    ch = getch();
                                                    if(ch == Escape){
                                                        free(changePasswordBoxes);
                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                        break;
                                                    }

                                                    if(ch == KEY_MOUSE) {
                                                        if(getmouse(&event) == OK){
                                                            if(event.bstate & BUTTON1_CLICKED) {
                                                                if(validClick(changePasswordBoxes[PasswordFieldChangePassword], event)){
                                                                    userPassword = setField(changePasswordBoxes[PasswordFieldChangePassword], Password, 8, userPassword);
                                                                }
                                                                else if(validClick(changePasswordBoxes[VoltarButtonChangePassword], event)){
                                                                    free(changePasswordBoxes);
                                                                    mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                    break;
                                                                }
                                                                else if(validClick(changePasswordBoxes[ButtonChangePassword], event)){
                                                                    int response;
                                                                    response = changePassword(userPassword, changePasswordBoxes[PasswordFieldChangePassword]);
                                                                    if(response == TRUE){
                                                                        halfdelay(10);
                                                                        getch();
                                                                        nocbreak();
                                                                        free(changePasswordBoxes);
                                                                        mainMenuBoxes = showMainMenu(paginacaoMaterias, paginacaoTarefas);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else if(validClick(mainMenuBoxes[LogoutButtonMainMenu], event)){
                                                free(mainMenuBoxes);
                                                loginBoxes = showLoginScreen();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if(validClick(loginBoxes[SignInButton], event)){
                        clear();
                        userLogin[0] = '\0';
                        userPassword[0] = '\0';
                        CoordinatePairs **signInBoxes;
                        char *userLoginSignIn = calloc(1,sizeof(char)), *userPasswordSignIn = calloc(1,sizeof(char)), *userNameSignIn = calloc(1,sizeof(char));
                        signInBoxes = showSignInScreen();
                        while(TRUE){
                            ch = getch();
                            if(ch == Escape){
                                free(signInBoxes);
                                loginBoxes = showLoginScreen();
                                break;
                            }

                            if(ch == KEY_MOUSE) {
                                if(getmouse(&event) == OK){
                                    if(event.bstate & BUTTON1_CLICKED) {
                                        if(validClick(signInBoxes[LoginFieldSignIn], event)){
                                            userLoginSignIn = setField(signInBoxes[LoginFieldSignIn], Text, 12, userLoginSignIn);
                                        }
                                        else if(validClick(signInBoxes[PasswordFieldSignIn], event)){
                                            userPasswordSignIn = setField(signInBoxes[PasswordFieldSignIn], Password, 8, userPasswordSignIn);
                                        }
                                        else if(validClick(signInBoxes[NameFieldSignIn], event)){
                                            userNameSignIn = setField(signInBoxes[NameFieldSignIn], Text, 30, userNameSignIn);
                                        }
                                        else if(validClick(signInBoxes[VoltarButtonSignIn], event)){
                                            free(signInBoxes);
                                            loginBoxes = showLoginScreen();
                                            break;
                                        }
                                        else if(validClick(signInBoxes[SignInButtonSignIn], event)){
                                            int validSignIn;
                                            validSignIn = signIn(userLoginSignIn, userPasswordSignIn, userNameSignIn, signInBoxes[LoginFieldSignIn], signInBoxes[PasswordFieldSignIn], signInBoxes[NameFieldSignIn]);
                                            if(validSignIn == TRUE){
                                                halfdelay(10);
                                                getch();
                                                nocbreak();
                                                free(signInBoxes);
                                                loginBoxes = showLoginScreen();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    free(loginBoxes);
    refresh();
    endwin();

	return 0;
}
