# Projeto Final da matéria AEDS opt CEFET-MG

O projeto "Agenda de atividades" tem como objetivo auxiliar o aluno a gerir as atividades do EAD, nela você pode:

- Fazer o Cadastro e gerir as suas matérias
- Fazer o Cadastro e gerir as suas atividades

A plataforma tambem tem um controle de prazo de entrega, e sempre te exibe primeiro as materias mais proximas deste prazo, para que você possa se organizar melhor sobre qual fazer.

## Grupo

- Gabriel Meneghin
- João Victor Cardoso de Lima
- Wander Bassi

## Build

- Abra o codeBlocks;
- Abra o Projeto;
- No "workspace", clique com o botao direito no projeto e então em "Build options";
- Navegue até "Linker settings" caso a biblioteca "pdcurses.a" não esteja presente clique am "add";
- Navegue até a pasta do projeto, no diretorio "lib" você encontrara a biblioteca "pdcurses";
- Selecione a biblioteca "pdcurses" e clique em abrir;
- Na Question Box que aparecer, selecione "sim";
- Se tudo foi feito corretamente, você sera capaz de dar build no Projeto;

## Run

- Navegue até a pasta do projeto;
- Entre no diretório "bin/Debug";
- Execute o aplicativo presente na pasta;

## Estrutura

O codigo fez uso da estrutura de Lista Simples, para alocar dinamicamente todos os arrays necessarios. A ideia original era ter uma interface
bem detalhada e interativa, então o primeiro desafio foi achar alguma biblioteca que fizesse isso em C, após analizar as opçoes, escolhemos a 
pdCurses, principalmente pela habilidade de mapear o clique do mouse, entrando e organizaçao, nos temos

### Main.c

Responsável por coordenar toda a aplicação

### Main.h

Responsável pela interface, todo o desenho das telas está aqui

### sigaa.h

Responsavel por dar os comandos para o file.h, alem de salvar os "cookies" da aplicação

### file.h

Responsável por se comunicar com os arquivos de texto

O mais desafiador deste projeto, foi faze-lo de forma que os arquivos de texto funcionassem como tabelas de um banco de dados, onde um
usuario pode ter varias materias, que podem ter varias atividades, com ids gerados dinamicamentes, e respeitando a segurança para que um
aluno logado não possa ver as informações de outra pessoa.

## Conclusão

Apesar de ter funcionado, ficou clara as limitaçoes do C e das estruturas escolhidas, vide o tempo necessario para debugar alguns erros, e a
quantidade de codigo necessaria para peformar açoes simples como criar uma "caixa" no CMD, porém ao mesmo tempo foi um grande desafio trabalhar
nesse projeto, e creio que ele irá auxiliar muito em projetos futuros, por ter melhorado o nosso entendimento nessa parte mais baixo nivel da
arquitetura do código


