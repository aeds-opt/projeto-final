#ifndef SIGAA_H_INCLUDED
#define SIGAA_H_INCLUDED

#include "file.h"

#define NotFound 0

const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

Aluno *AlunoLogado;
ArrayAtividades *atividades;
int materiasArraySize;
int atividadesArraySize, atividadesDaMateriaArraySize;

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_encoded_size(size_t inlen)
{
	size_t ret;

	ret = inlen;
	if (inlen % 3 != 0)
		ret += 3 - (inlen % 3);
	ret /= 3;
	ret *= 4;

	return ret;
}

char *b64_encode(const unsigned char *in, size_t len)
{
	char   *out;
	size_t  elen;
	size_t  i;
	size_t  j;
	size_t  v;

	if (in == NULL || len == 0)
		return NULL;

	elen = b64_encoded_size(len);
	out  = malloc(elen+1);
	out[elen] = '\0';

	for (i=0, j=0; i<len; i+=3, j+=4) {
		v = in[i];
		v = i+1 < len ? v << 8 | in[i+1] : v << 8;
		v = i+2 < len ? v << 8 | in[i+2] : v << 8;

		out[j]   = b64chars[(v >> 18) & 0x3F];
		out[j+1] = b64chars[(v >> 12) & 0x3F];
		if (i+1 < len) {
			out[j+2] = b64chars[(v >> 6) & 0x3F];
		} else {
			out[j+2] = '=';
		}
		if (i+2 < len) {
			out[j+3] = b64chars[v & 0x3F];
		} else {
			out[j+3] = '=';
		}
	}

	return out;
}

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}

void setAlunoLogado(Aluno *currentAluno){
    AlunoLogado = calloc(1, sizeof(Aluno));

    AlunoLogado->numMatricula = calloc(strlen(currentAluno->numMatricula), sizeof(char));
    AlunoLogado->senha = calloc(strlen(currentAluno->senha), sizeof(char));
    AlunoLogado->nome = calloc(strlen(currentAluno->nome), sizeof(char));

    AlunoLogado->id = currentAluno->id;
    strcat(AlunoLogado->numMatricula, currentAluno->numMatricula);
    strcat(AlunoLogado->senha, currentAluno->senha);
    strcat(AlunoLogado->nome, currentAluno->nome);
}

Aluno *getAlunoLogado(){
    return AlunoLogado;
}

void setMateriasArraySize(int newSize){
    materiasArraySize = newSize;
}

int getMateriasArraySize(){
    return materiasArraySize;
}

void setAtividadesArraySize(int newSize){
    atividadesArraySize = newSize;
}

int getAtividadesArraySize(){
    return atividadesArraySize;
}

void setAtividadesDaMateriaArraySize(int newSize){
    atividadesDaMateriaArraySize = newSize;
}

int getAtividadesDaMateriaArraySize(){
    return atividadesDaMateriaArraySize;
}

void setArrayAtividades(ArrayAtividades *newArrayAtividades){
    atividades = calloc(1, sizeof(ArrayAtividades));

    atividades->size = newArrayAtividades->size;
    atividades->used = newArrayAtividades->used;

    atividades->atividades = calloc(atividades->size,sizeof(Atividade));
    atividades->atividades = newArrayAtividades->atividades;

}

ArrayAtividades *getArrayAtividades(){
    return atividades;
}


Aluno *getAluno(char *login, char *password){
    Aluno *currentAluno;
    currentAluno = getAlunoFromFile(login, password);

    if(currentAluno->id > NotFound){
        char *base64Password;
        base64Password = b64_encode(password, strlen(password));
        if(strcmp(base64Password, currentAluno->senha) == 0){
            setAlunoLogado(currentAluno);
            return currentAluno;
        }
        else{
            currentAluno->senha = calloc(1, sizeof(char));
            currentAluno->senha = '0';
            return currentAluno;
        }
    }
    else{
        return currentAluno;
    }
}

Aluno *postAluno(char *login, char *password, char* name){
    Aluno *newAluno;
    char *base64Password;

    base64Password = b64_encode(password, strlen(password));

    newAluno = postAlunoOnFile(login, base64Password, name);

    return newAluno;

}

Aluno *updateAluno(char *newPassword){
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    ArrayAlunos *alunos;
    alunos = getAllAlunosFromFile();

    char *base64Password;

    base64Password = b64_encode(newPassword, strlen(newPassword));

    int aux;
    for(aux = 0; aux < alunos->size; aux++){
        if(alunos->alunos[aux].id == currentAluno->id){
           alunos->alunos[aux].senha = base64Password;
           currentAluno->senha = base64Password;
        }
    }

    updateAlunosOnFile(alunos);

    setAlunoLogado(currentAluno);

    return currentAluno;

}

ArrayMaterias *getMateriasOfCurrentUser(){
    ArrayMaterias *materias;
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    materias = getMateriasFromFile(currentAluno->id);

    setMateriasArraySize(materias->size);

    return materias;

}

Materia *postMateria(char *name){
    Materia *newMateria;
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    newMateria = postMateriaOnFile(name, currentAluno->id);

    return newMateria;

}

ArrayMaterias *updateMaterias(ArrayMaterias *currentUserMaterias){

    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    ArrayMaterias *allMaterias;
    ArrayMaterias *newArrayMaterias;

    allMaterias = getAllMateriasFromFile();


    newArrayMaterias = calloc(1, sizeof(ArrayMaterias));
    newArrayMaterias->materias = calloc(1,sizeof(Materia));
    newArrayMaterias->size = 1;
    newArrayMaterias->used = 0;

    int aux,aux2;

    for(aux = 0; aux < allMaterias->size; aux++){
        if(allMaterias->materias[aux].userId == currentAluno->id){
            for(aux2 = 0; aux2 < currentUserMaterias->size; aux2++){
                if(allMaterias->materias[aux].id == currentUserMaterias->materias[aux2].id){
                    if (newArrayMaterias->used == newArrayMaterias->size) {
                        newArrayMaterias->size += 1;
                        newArrayMaterias->materias = realloc(newArrayMaterias->materias, newArrayMaterias->size * sizeof(Materia));
                    }

                    newArrayMaterias->materias[newArrayMaterias->used++] = currentUserMaterias->materias[aux2];
                }
            }
        }
        else{
            if (newArrayMaterias->used == newArrayMaterias->size) {
                newArrayMaterias->size += 1;
                newArrayMaterias->materias = realloc(newArrayMaterias->materias, newArrayMaterias->size * sizeof(Materia));
            }

            newArrayMaterias->materias[newArrayMaterias->used++] = allMaterias->materias[aux];
        }
    }
    updateMateriasOnFile(newArrayMaterias);

    setMateriasArraySize(currentUserMaterias->size);

    return currentUserMaterias;

}

ArrayAtividades *getAtividadesOfCurrentUser(){
    ArrayAtividades *atividades;
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    atividades = getAtividadesFromFile(currentAluno->id, FALSE);

    setAtividadesArraySize(atividades->size);

    return atividades;

}

ArrayAtividades *getAtividadesOfCurrentUserandMateria(int materiaId){
    ArrayAtividades *atividades;
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    atividades = getAtividadesFromFile(currentAluno->id, materiaId);

    setAtividadesDaMateriaArraySize(atividades->size);

    return atividades;

}

Atividade *postAtividade(char *name, char *descricao, char *valor, char *data, char *materia, int materiaId){
    Atividade *newAtividade;
    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    newAtividade = postAtividadeOnFile(name, descricao, valor, data, materia, currentAluno->id, materiaId);

    return newAtividade;

}

ArrayAtividades *updateAtividades(ArrayAtividades *currentUserAtividades){

    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    ArrayAtividades *allAtividades;
    ArrayAtividades *newArrayAtividades;

    allAtividades = getAllAtividadesFromFile();

    newArrayAtividades = calloc(1, sizeof(ArrayAtividades));
    newArrayAtividades->atividades = calloc(1,sizeof(Atividade));
    newArrayAtividades->size = 1;
    newArrayAtividades->used = 0;

    int aux,aux2;

    for(aux = 0; aux < allAtividades->size; aux++){
        if(allAtividades->atividades[aux].userId == currentAluno->id){
            for(aux2 = 0; aux2 < currentUserAtividades->size; aux2++){
                if(allAtividades->atividades[aux].id == currentUserAtividades->atividades[aux2].id){
                    if (newArrayAtividades->used == newArrayAtividades->size) {
                        newArrayAtividades->size += 1;
                        newArrayAtividades->atividades = realloc(newArrayAtividades->atividades, newArrayAtividades->size * sizeof(Atividade));
                    }

                    newArrayAtividades->atividades[newArrayAtividades->used++] = currentUserAtividades->atividades[aux2];
                }
            }
        }
        else{
            if (newArrayAtividades->used == newArrayAtividades->size) {
                newArrayAtividades->size += 1;
                newArrayAtividades->atividades = realloc(newArrayAtividades->atividades, newArrayAtividades->size * sizeof(Atividade));
            }

            newArrayAtividades->atividades[newArrayAtividades->used++] = allAtividades->atividades[aux];
        }
    }

    int auxOrd1, auxOrd2, auxStatus;
    Atividade placeholder;

    for (auxOrd1 = 0; auxOrd1 < newArrayAtividades->size; auxOrd1++){
        for (auxOrd2 = 0; auxOrd2 < newArrayAtividades->size; auxOrd2++) {
            if(newArrayAtividades->atividades[auxOrd1].id < newArrayAtividades->atividades[auxOrd2].id){
                placeholder = newArrayAtividades->atividades[auxOrd1];
                newArrayAtividades->atividades[auxOrd1] = newArrayAtividades->atividades[auxOrd2];
                newArrayAtividades->atividades[auxOrd2] = placeholder;
            }
        }
    }

    updateAtividadesOnFile(newArrayAtividades);

    setAtividadesArraySize(currentUserAtividades->size);

    setArrayAtividades(currentUserAtividades);

    return currentUserAtividades;

}

#endif // SIGAA_H_INCLUDED
