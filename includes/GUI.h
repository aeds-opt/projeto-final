#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#include <time.h>
#include <string.h>
#include "sigaa.h"

#define Width 119
#define Height 29

#define White 1
#define Blue 2
#define Green 3
#define Red 4
#define Yellow 5
#define Black 6
#define Success 7
#define Danger 8

#define Text 0
#define Password 1
#define Number 2
#define Data 3

#define BackSpace 8
#define Enter 13
#define Escape 27

#define Pendente 0
#define Entregue 1
#define Atrasada 2
#define Atencao 3
#define Normal 4

#define MotivoAtraso -1
#define MotivoEntregue 0
#define Sucesso 1

#define Nome 0
#define Descricao 1
#define Valor 2
#define Data 3

typedef struct {
    int column;
    int line;
}CoordinatePairs;

void initPairs(){
    init_pair(White, COLOR_WHITE, COLOR_BLACK);
    init_pair(Blue, COLOR_BLUE, COLOR_BLACK);
    init_pair(Green, COLOR_GREEN, COLOR_BLACK);
    init_pair(Red, COLOR_RED, COLOR_BLACK);
    init_pair(Yellow, COLOR_YELLOW, COLOR_BLACK);
    init_pair(Black, COLOR_BLACK, COLOR_WHITE);
    init_pair(Success, COLOR_WHITE, COLOR_GREEN);
    init_pair(Danger, COLOR_WHITE, COLOR_RED);
}

void initCursor(){
    initscr();
    raw();
    noecho();
    keypad(stdscr, TRUE);
	curs_set(FALSE);
    nocbreak();
	start_color();
	initPairs();
}

void drawLine(int initialWidth, int finalWidth, int line, int style){
    for(int aux = initialWidth; aux <= finalWidth; aux++){
        attron(COLOR_PAIR(style));
        mvprintw(line, aux,"_");
        attroff(COLOR_PAIR(style));
    }
}

void drawColunm(int initialHeight, int finalHeight, int column, int style){
    for(int aux = initialHeight; aux <= finalHeight; aux++){
        attron(COLOR_PAIR(style));
        mvprintw(aux, column,"|");
        attroff(COLOR_PAIR(style));
    }
}

CoordinatePairs *drawBox(int width, int height, int vertexALine, int vertexAColumn, int style){
    CoordinatePairs *vertexBox = calloc(4, sizeof(CoordinatePairs));

    vertexBox[0].column = vertexAColumn;
    vertexBox[0].line = vertexALine;

    vertexBox[1].column = vertexAColumn + width;
    vertexBox[1].line = vertexALine;

    vertexBox[2].column = vertexAColumn;
    vertexBox[2].line = vertexALine + height;

    vertexBox[3].column = vertexAColumn + width;
    vertexBox[3].line = vertexALine + height;

    drawLine(vertexAColumn, (vertexAColumn + width), vertexALine, style);
    drawLine(vertexAColumn, (vertexAColumn + width), (vertexALine + height), style);
    drawColunm((vertexALine + 1), (vertexALine + height), vertexAColumn, style);
    drawColunm((vertexALine + 1), (vertexALine + height), (vertexAColumn + width), style);

    return vertexBox;
}

void printVertex(CoordinatePairs *vertexBox){
    printw("\n");
    for(int aux = 0; aux < 4; aux++){
        printw("Vertice: %i -> (%i,%i)\n", aux+1, vertexBox[aux].column, vertexBox[aux].line);
    }
}

int validClick(CoordinatePairs *vertexBox, MEVENT click){
    if((vertexBox[0].line <= click.y) && (vertexBox[2].line >= click.y)){
        if((vertexBox[0].column <= click.x) && (vertexBox[1].column >= click.x)){
            return TRUE;
        }
    }

    return FALSE;
}

char *setField(CoordinatePairs *vertexBox, int type, int maxLenght, char *currentValue){
    int length, height = vertexBox[2].line - vertexBox[0].line, width = vertexBox[1].column - vertexBox[0].column;
    int middleLine = ((vertexBox[0].line + vertexBox[2].line)/2);
    int toggle = FALSE;
    int count = 0;
    char *userInput, *userInputSecret;
    int input;
    int firstInput = TRUE, hasDot = FALSE;

    for(int aux = -5; aux <= (vertexBox[1].column - vertexBox[0].column + 5); aux++){
        attron(COLOR_PAIR(White));
        mvprintw(vertexBox[2].line + 1, vertexBox[0].column + aux, " ");
        attroff(COLOR_PAIR(White));
    }

    if(maxLenght == FALSE){
        length = 40;
    }
    else{
        if(type == Data){
         length = 10;
        }
        else{
         length = maxLenght+2;
        }
    }

    userInput = calloc(length,sizeof(char));
    userInputSecret = calloc(length,sizeof(char));

    strcat(userInput,currentValue);
    count += strlen(currentValue);

    for(int aux = 0; aux < count; aux++){
        userInputSecret[aux] = '*';
    }


    drawBox(width, height, vertexBox[0].line, vertexBox[0].column, Blue);
    halfdelay(5);

    while (TRUE){
        toggle = !toggle;
        if(toggle){
            mvprintw(middleLine, vertexBox[0].column + 1 + count, "|");
        }
        else{
            mvprintw(middleLine, vertexBox[0].column + 1 + count, " ");
        }
        input = getch();
        if(input != ERR){
            if(input == Enter || input == Escape){
                break;
            }
            else if(input == BackSpace){
                    if(count > 0){
                        mvprintw(middleLine, vertexBox[0].column + 1 + count, " ");
                        count--;
                        if(userInput[count] == '.'){
                            hasDot = FALSE;
                        }
                        userInput[count] = NULL;
                        if(type == Password){
                            userInputSecret[count] = NULL;
                        }
                        if(count == 0){
                            firstInput = TRUE;
                        }
                    }
            }
            else if(input >= 0 && input <= 127){
                if(maxLenght == FALSE){
                    if(count >= length){
                        userInput = realloc(userInput, (length *= 10) * sizeof(char));
                        userInputSecret = realloc(userInput, (length *= 10) * sizeof(char));
                    }
                    if(type == Password){
                        userInputSecret[count] = '*';
                    }
                    if(type == Number){
                        if((input == 48)&&(firstInput == FALSE)){
                            userInput[count++] = (char)input;
                        }
                        else if((input >= 49)&&(input <= 57)){
                            userInput[count++] = (char)input;
                            firstInput = FALSE;
                        }
                        else if(((input == 44)||(input == 46))&&(hasDot == FALSE)){
                            hasDot = TRUE;
                            if((firstInput == TRUE)&&(maxLenght >= 3)){
                                userInput[count++] = '0';
                                userInput[count++] = '.';
                                firstInput = FALSE;
                            }
                            else{
                                userInput[count++] = '.';
                                firstInput = FALSE;
                            }
                        }
                    }
                    else{
                      userInput[count++] = (char)input;
                    }
                }
                else{
                    if(count < maxLenght){
                        if(type == Password){
                            userInputSecret[count] = '*';
                        }
                        if(type == Number){
                            if((input == 48)&&(firstInput == FALSE)){
                                userInput[count++] = (char)input;
                            }
                            else if((input >= 49)&&(input <= 57)){
                                userInput[count++] = (char)input;
                                firstInput = FALSE;
                            }
                            else if(((input == 44)||(input == 46))&&(hasDot == FALSE)){
                                hasDot = TRUE;
                                if((firstInput == TRUE)&&(maxLenght >= 3)){
                                    userInput[count++] = '0';
                                    userInput[count++] = '.';
                                    firstInput = FALSE;
                                }
                                else{
                                    userInput[count++] = '.';
                                    firstInput = FALSE;
                                }
                            }
                        }
                        else if(type == Data){
                            if((input >= 48)&&(input <= 57)){
                                userInput[count++] = (char)input;
                                if((count == 2)||(count == 5)){
                                    userInput[count++] = '/';
                                }
                            }
                        }
                        else{
                          userInput[count++] = (char)input;
                        }
                    }
                }
            }
            else {
                break;
            }
        }
        if(type != Password){
            mvprintw(middleLine, vertexBox[0].column + 1, "%s", userInput);
        }
        else{
            mvprintw(middleLine, vertexBox[0].column + 1, "%s", userInputSecret);
        }
        refresh();
    }
    nocbreak();

    mvprintw(middleLine, vertexBox[0].column + 1 + count, " ");
    drawBox(width, height, vertexBox[0].line, vertexBox[0].column, White);
    free(userInputSecret);

    return userInput;
}

int login(char *login, char* password, CoordinatePairs *loginField, CoordinatePairs *passwordField){
    int validFields = TRUE;
    if(strlen(login) == 0 || strlen(login) < 12){
        attron(COLOR_PAIR(Danger));
        mvprintw(loginField[2].line + 1, loginField[0].column + (((loginField[1].column - loginField[0].column) - 14)/2), "Login invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(password) == 0 || strlen(password) < 8){
        attron(COLOR_PAIR(Danger));
        mvprintw(passwordField[2].line + 1, passwordField[0].column + (((passwordField[1].column - passwordField[0].column) - 14)/2), "Senha invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }

    if(validFields == TRUE){
         Aluno *currentAluno;
         currentAluno = getAluno(login,password);

         if(currentAluno->id == 0){
            attron(COLOR_PAIR(Danger));
            mvprintw(loginField[2].line + 1, loginField[0].column + (((loginField[1].column - loginField[0].column) - 24)/2), "Matricula nao encontrada");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(currentAluno);
            return FALSE;
         }
         else if(currentAluno->senha == '0'){
            attron(COLOR_PAIR(Danger));
            mvprintw(passwordField[2].line + 1, passwordField[0].column + (((passwordField[1].column - passwordField[0].column) - 15)/2), "Senha Incorreta");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(currentAluno);
            return FALSE;
         }
         else{
            attron(COLOR_PAIR(Success));
            mvprintw( Height, (Width - 23)/2 + 1 , "Login feito com sucesso");
            attroff(COLOR_PAIR(Success));
            refresh();
            free(currentAluno);
            return TRUE;
         }

    }
    else{
        return FALSE;
    }
}

int signIn(char *login, char* password, char* name, CoordinatePairs *loginField, CoordinatePairs *passwordField, CoordinatePairs *nameField){
    int validFields = TRUE;
    if(strlen(login) == 0 || strlen(login) < 12){
        attron(COLOR_PAIR(Danger));
        mvprintw(loginField[2].line + 1, loginField[0].column + (((loginField[1].column - loginField[0].column) - 12)/2), "Login invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(password) == 0 || strlen(password) < 8){
        attron(COLOR_PAIR(Danger));
        mvprintw(passwordField[2].line + 1, passwordField[0].column + (((passwordField[1].column - passwordField[0].column) - 12)/2), "Senha invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(name) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(nameField[2].line + 1, nameField[0].column + (((nameField[1].column - nameField[0].column) - 10)/2), "Nome invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }

    if(validFields == TRUE){
        Aluno *newAluno;
        newAluno = postAluno(login,password, name);

        if(newAluno->id == 0){
            attron(COLOR_PAIR(Danger));
            mvprintw( Height, (Width - 25)/2 + 1 , "Falha ao fazer o Cadastro");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(newAluno);
            return FALSE;
        }
        if(newAluno->numMatricula == "0"){
            attron(COLOR_PAIR(Danger));
            mvprintw( Height, (Width - 19)/2 + 1 , "Aluno ja cadastrado");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(newAluno);
            return FALSE;
        }
        else{
            attron(COLOR_PAIR(Success));
            mvprintw( Height, (Width - 26)/2 + 1 , "Cadastro feito com sucesso");
            attroff(COLOR_PAIR(Success));
            free(newAluno);
            return TRUE;
        }

    }
    else{
        return FALSE;
    }
}

int cadastrarMateria(char* name, CoordinatePairs *nameField){
    int validFields = TRUE;

    if(strlen(name) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(nameField[2].line + 1, nameField[0].column + (((nameField[1].column - nameField[0].column) - 10)/2), "Nome invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }

    if(validFields == TRUE){
        Materia *newMateria;
        newMateria = postMateria(name);

        if(newMateria->id == 0){
            attron(COLOR_PAIR(Danger));
            mvprintw( Height, (Width - 25)/2 + 1 , "Falha ao fazer o Cadastro");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(newMateria);
            return FALSE;
        }
        if(newMateria->nome == ""){
            attron(COLOR_PAIR(Danger));
            mvprintw( Height, (Width - 21)/2 + 1 , "Materia ja cadastrada");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(newMateria);
            return FALSE;
        }
        else{
            attron(COLOR_PAIR(Success));
            mvprintw( Height, (Width - 26)/2 + 1 , "Cadastro feito com sucesso");
            attroff(COLOR_PAIR(Success));
            free(newMateria);
            return TRUE;
        }

    }
    else{
        return FALSE;
    }
}

int cadastrarAtividade(char* name, char* descricao, char* valor, char* data, int indexMateria, CoordinatePairs *nameField, CoordinatePairs *descricaoField, CoordinatePairs *valorField, CoordinatePairs *dataField){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int validFields = TRUE;

    if(strlen(name) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(nameField[2].line + 1, nameField[0].column + (((nameField[1].column - nameField[0].column) - 10)/2), "Nome invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(descricao) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(descricaoField[2].line + 1, descricaoField[0].column + (((descricaoField[1].column - descricaoField[0].column) - 18)/2), "Descricao invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(valor) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(valorField[2].line + 1, valorField[0].column + (((valorField[1].column - valorField[0].column) - 11)/2), "Valor invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    else if(atof(valor) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(valorField[2].line + 1, valorField[0].column + (((valorField[1].column - valorField[0].column) - 11)/2), "Valor invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(data) < 10){
        attron(COLOR_PAIR(Danger));
        mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    else{
        char dia[3], mes[3], ano[5];

        dia[0] = data[0];
        dia[1] = data[1];
        dia[2] = '\0';
        mes[0] = data[3];
        mes[1] = data[4];
        mes[2] = '\0';
        ano[0] = data[6];
        ano[1] = data[7];
        ano[2] = data[8];
        ano[3] = data[9];
        ano[4] = '\0';


        if(atoll(dia) > 0 && atoll(mes) > 0 && atoll(ano) > 0){
            if(atoll(dia) > 31){
                attron(COLOR_PAIR(Danger));
                mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
                attroff(COLOR_PAIR(Danger));
                refresh();
                validFields = FALSE;
            }
            else if(atoll(mes) > 12){
                attron(COLOR_PAIR(Danger));
                mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
                attroff(COLOR_PAIR(Danger));
                refresh();
                validFields = FALSE;
            }
            else{
                if(atoll(ano) < tm.tm_year + 1900){
                    attron(COLOR_PAIR(Danger));
                    mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Ano invalido");
                    attroff(COLOR_PAIR(Danger));
                    refresh();
                    validFields = FALSE;
                }
                else{
                    if(atoll(ano) <= tm.tm_year + 1900){
                        if(atoll(mes) < tm.tm_mon + 1){
                            attron(COLOR_PAIR(Danger));
                            mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Mes invalido");
                            attroff(COLOR_PAIR(Danger));
                            refresh();
                            validFields = FALSE;
                        }
                        else{
                            if(atoll(mes) <= tm.tm_mon + 1){
                                if(atoll(dia) < tm.tm_mday){
                                    attron(COLOR_PAIR(Danger));
                                    mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Dia invalido");
                                    attroff(COLOR_PAIR(Danger));
                                    refresh();
                                    validFields = FALSE;
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            attron(COLOR_PAIR(Danger));
            mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
            attroff(COLOR_PAIR(Danger));
            refresh();
            validFields = FALSE;
        }


    }

    if(validFields == TRUE){
        ArrayMaterias *materias;
        materias = getMateriasOfCurrentUser();

        Atividade *newAtividade;
        newAtividade = postAtividade(name, descricao, valor, data, materias->materias[indexMateria].nome, materias->materias[indexMateria].id);

        if(newAtividade->id == 0){
            attron(COLOR_PAIR(Danger));
            mvprintw( Height, (Width - 25)/2 + 1 , "Falha ao fazer o Cadastro");
            attroff(COLOR_PAIR(Danger));
            refresh();
            free(newAtividade);
            return FALSE;
        }
        else{
            attron(COLOR_PAIR(Success));
            mvprintw( Height, (Width - 26)/2 + 1 , "Cadastro feito com sucesso");
            attroff(COLOR_PAIR(Success));
            free(newAtividade);
            return TRUE;
        }

    }
    else{
        return FALSE;
    }
}

CoordinatePairs **showLoginScreen(){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **loginBoxes = (CoordinatePairs **)calloc(4, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexLogin, *vertexPasssword, *vertexButtonLogin, *vertexButtonSignIn;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);
    drawColunm(headerLine, Height,(headerColumn - 1), White);
    drawColunm(headerLine, Height,(headerColumn + 41 + 1), White);


    attron(COLOR_PAIR(White));
    mvprintw(8, ((headerColumn + 11 + 1)+((20 - 9)/2)),"Matricula");
    attroff(COLOR_PAIR(White));

    vertexLogin = drawBox(20, 2, 9, (headerColumn + 11), White);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((headerColumn + 11 + 1)+((20 - 5)/2)),"Senha");
    attroff(COLOR_PAIR(White));

    vertexPasssword = drawBox(20, 2, 15, (headerColumn + 11), White);

    vertexButtonLogin = drawBox(20, 3, 20, (headerColumn + 11), White);

    attron(COLOR_PAIR(White));
    mvprintw(22, ((headerColumn + 11 + 1)+((20 - 5)/2)),"Login");
    attroff(COLOR_PAIR(White));

    vertexButtonSignIn = drawBox(20, 3, 24, (headerColumn + 11), White);

    attron(COLOR_PAIR(White));
    mvprintw(26, ((headerColumn + 11 + 1)+((20 - 9)/2)),"Cadastrar");
    attroff(COLOR_PAIR(White));

    loginBoxes[0] = vertexLogin;
    loginBoxes[1] = vertexPasssword;
    loginBoxes[2] = vertexButtonLogin;
    loginBoxes[3] = vertexButtonSignIn;

    return loginBoxes;
}

CoordinatePairs **showSignInScreen(){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **signInBoxes = (CoordinatePairs **)calloc(5, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexLogin, *vertexPasssword, *vertexName, *vertexButtonVoltar, *vertexButtonSignIn;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    attron(COLOR_PAIR(White));
    mvprintw(8, ((headerColumn + 1 + 1)+((40 - 9)/2)),"Matricula");
    attroff(COLOR_PAIR(White));

    vertexLogin = drawBox(40, 2, 9, (headerColumn + 1), White);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((headerColumn + 1 + 1)+((40 - 5)/2)),"Senha");
    attroff(COLOR_PAIR(White));

    vertexPasssword = drawBox(40, 2, 15, (headerColumn + 1), White);

    attron(COLOR_PAIR(White));
    mvprintw(20, ((headerColumn + 1 + 1)+((40 - 4)/2)),"Nome");
    attroff(COLOR_PAIR(White));

    vertexName = drawBox(40, 2, 21, (headerColumn + 1), White);

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonSignIn = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Cadastrar");
    attroff(COLOR_PAIR(White));

    signInBoxes[0] = vertexLogin;
    signInBoxes[1] = vertexPasssword;
    signInBoxes[2] = vertexName;
    signInBoxes[3] = vertexButtonVoltar;
    signInBoxes[4] = vertexButtonSignIn;

    return signInBoxes;
}

CoordinatePairs **showMainMenu(int paginacaoMaterias, int paginacaoAtividade){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **mainMenuBoxes = (CoordinatePairs **)calloc(14, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexButtonMateria1, *vertexButtonMateria2, *vertexButtonMateria3, *vertexButtonNextPageMaterias, *vertexButtonPreviousPageMaterias, *vertexButtonCadastrarMateria, *vertexButtonCadastrarAtividade, *vertexButtonEditarMatricula, *vertexButtonLogout;
    CoordinatePairs *vertexButtonAtividade, *vertexButtonAtividade2, *vertexButtonAtividade3, *vertexButtonPreviousPageAtividades, *vertexButtonNextPageAtividades;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);
    drawColunm(headerLine, Height,(headerColumn - 1), White);
    drawColunm(headerLine, Height,(headerColumn + 41 + 1), White);

    //Materias----------------------------------------------------------------------------------------------------------------------------------------

    int materiasLine = headerLine, materiasColumn = 2;

    attron(COLOR_PAIR(White));
    mvprintw(materiasLine++, materiasColumn,"          ___  ___  __          __ ");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(materiasLine++, materiasColumn,"|\\/|  /\\   |  |__  |__) |  /\\  /__`");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(materiasLine++, materiasColumn,"|  | /~~\\  |  |___ |  \\ | /~~\\ .__/ ");
    attroff(COLOR_PAIR(White));

    ArrayMaterias *materias;

    materias = getMateriasOfCurrentUser();

    int limiteSuperiorIndexMateria = paginacaoMaterias * 3;
    int limiteInferiorIndexMateria = limiteSuperiorIndexMateria - 3;

    if(limiteSuperiorIndexMateria > materias->size){
        limiteSuperiorIndexMateria = materias->size;
    }

    if(limiteInferiorIndexMateria < 0){
        limiteInferiorIndexMateria = 0;
    }

    int firstMateria = materiasLine+=2;
    int materia1Label, materia2Label, materia3Label;

    vertexButtonMateria1 = drawBox(33, 3, firstMateria, (materiasColumn), White);
    materia1Label = firstMateria + 2;

    vertexButtonMateria2 = drawBox(33, 3, firstMateria+=4, (materiasColumn), White);
    materia2Label = firstMateria + 2;

    vertexButtonMateria3 = drawBox(33, 3, firstMateria+=4, (materiasColumn), White);
    materia3Label = firstMateria + 2;

    int aux = 0, index = 0;

    for(aux = limiteInferiorIndexMateria; aux < limiteSuperiorIndexMateria; aux++){
        if(index == 0){
            attron(COLOR_PAIR(White));
            mvprintw(materia1Label, ((materiasColumn + 1)+((33 - strlen(materias->materias[aux].nome))/2)),"%s",materias->materias[aux].nome);
            attroff(COLOR_PAIR(White));
        }
        else if(index == 1){
            attron(COLOR_PAIR(White));
            mvprintw(materia2Label, ((materiasColumn + 1)+((33 - strlen(materias->materias[aux].nome))/2)),"%s",materias->materias[aux].nome);
            attroff(COLOR_PAIR(White));
        }
        else if(index == 2){
            attron(COLOR_PAIR(White));
            mvprintw(materia3Label, ((materiasColumn + 1)+((33 - strlen(materias->materias[aux].nome))/2)),"%s",materias->materias[aux].nome);
            attroff(COLOR_PAIR(White));
        }
        index++;
    }

    vertexButtonPreviousPageMaterias = drawBox(10, 4, (Height - 4), 0, White);

    attron(COLOR_PAIR(White));
    mvprintw((Height - 4) + 2, ((0 + 1)+((10 - 1)/2)),"/");
    mvprintw((Height - 4) + 3, ((0 + 1)+((10 - 1)/2)),"\\");
    attroff(COLOR_PAIR(White));

    vertexButtonNextPageMaterias = drawBox(10, 4, (Height - 4), 27, White);

    attron(COLOR_PAIR(White));
    mvprintw((Height - 4) + 2, ((27 + 1)+((10 - 1)/2)),"\\");
    mvprintw((Height - 4) + 3, ((27 + 1)+((10 - 1)/2)),"/");
    attroff(COLOR_PAIR(White));

    //Atividades----------------------------------------------------------------------------------------------------------------------------------------

    int atividadesLine = headerLine, atividadesColumn = headerColumn;

    attron(COLOR_PAIR(White));
    mvprintw(atividadesLine++, atividadesColumn,"     ___           __        __   ___  __ ");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(atividadesLine++, atividadesColumn," /\\   |  | \\  / | |  \\  /\\  |  \\ |__  /__`");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(atividadesLine++, atividadesColumn,"/~~\\  |  |  \\/  | |__/ /~~\\ |__/ |___ .__/");
    attroff(COLOR_PAIR(White));

    ArrayAtividades *atividades;

    atividades = getAtividadesOfCurrentUser();

    int auxOrd1, auxOrd2, auxStatus;
    Atividade placeholder;
    unsigned int dataAux1, dataAux2;
    char data1[9], data2[9];

    for (auxOrd1 = 0; auxOrd1 < atividades->size; auxOrd1++){
        for (auxOrd2 = 0; auxOrd2 < atividades->size; auxOrd2++) {
            data1[0] = atividades->atividades[auxOrd1].data[6];
            data1[1] = atividades->atividades[auxOrd1].data[7];
            data1[2] = atividades->atividades[auxOrd1].data[8];
            data1[3] = atividades->atividades[auxOrd1].data[9];
            data1[4] = atividades->atividades[auxOrd1].data[3];
            data1[5] = atividades->atividades[auxOrd1].data[4];
            data1[6] = atividades->atividades[auxOrd1].data[0];
            data1[7] = atividades->atividades[auxOrd1].data[1];
            data1[8] = '\0';
            data2[0] = atividades->atividades[auxOrd2].data[6];
            data2[1] = atividades->atividades[auxOrd2].data[7];
            data2[2] = atividades->atividades[auxOrd2].data[8];
            data2[3] = atividades->atividades[auxOrd2].data[9];
            data2[4] = atividades->atividades[auxOrd2].data[3];
            data2[5] = atividades->atividades[auxOrd2].data[4];
            data2[6] = atividades->atividades[auxOrd2].data[0];
            data2[7] = atividades->atividades[auxOrd2].data[1];
            data2[8] = '\0';

            dataAux1 = atoll(data1),
            dataAux2 = atoll(data2);

            if(dataAux1 < dataAux2){
                placeholder = atividades->atividades[auxOrd1];
                atividades->atividades[auxOrd1] = atividades->atividades[auxOrd2];
                atividades->atividades[auxOrd2] = placeholder;
            }
        }
    }

    for (auxStatus = 0; auxStatus < atividades->size; auxStatus++){
        data1[0] = atividades->atividades[auxStatus].data[6];
        data1[1] = atividades->atividades[auxStatus].data[7];
        data1[2] = atividades->atividades[auxStatus].data[8];
        data1[3] = atividades->atividades[auxStatus].data[9];
        data1[4] = atividades->atividades[auxStatus].data[3];
        data1[5] = atividades->atividades[auxStatus].data[4];
        data1[6] = atividades->atividades[auxStatus].data[0];
        data1[7] = atividades->atividades[auxStatus].data[1];
        data1[8] = '\0';

        char year[5], month[3], day[3];

        sprintf(year, "%d", tm.tm_year + 1900);

        if((tm.tm_mon + 1) < 10){
           sprintf(month, "0%d", tm.tm_mon + 1);
        }
        else{
          sprintf(month, "%d", tm.tm_mon + 1);
        }

        if(tm.tm_mday < 10){
           sprintf(day, "0%d", tm.tm_mday);
        }
        else{
          sprintf(day, "%d", tm.tm_mday);
        }

        data2[0] = year[0];
        data2[1] = year[1];
        data2[2] = year[2];
        data2[3] = year[3];
        data2[4] = month[0];
        data2[5] = month[1];
        data2[6] = day[0];
        data2[7] = day[1];
        data2[8] = '\0';

        dataAux1 = atoll(data1);
        dataAux2 = atoll(data2);

        if(atividades->atividades[auxStatus].status == Pendente){
            if(dataAux1 > dataAux2){
                atividades->atividades[auxStatus].status = Normal;
            }
            else if(dataAux1 == dataAux2){
                atividades->atividades[auxStatus].status = Atencao;
            }
            else{
                atividades->atividades[auxStatus].status = Atrasada;
            }
        }
    }

    setArrayAtividades(atividades);

    int limiteSuperiorIndexAtividade = paginacaoAtividade * 3;
    int limiteInferiorIndexAtividade = limiteSuperiorIndexAtividade - 3;

    if(limiteSuperiorIndexAtividade > atividades->size){
        limiteSuperiorIndexAtividade = atividades->size;
    }

    if(limiteInferiorIndexAtividade < 0){
        limiteInferiorIndexAtividade = 0;
    }

    int firstAtividade = atividadesLine+=2;
    int atividade1Label, atividade2Label, atividade3Label;

    vertexButtonAtividade = drawBox(37, 3, firstAtividade, (atividadesColumn + 2), White);
    atividade1Label = firstAtividade + 2;

    vertexButtonAtividade2 = drawBox(37, 3, firstAtividade+=4, (atividadesColumn + 2), White);
    atividade2Label = firstAtividade + 2;

    vertexButtonAtividade3 = drawBox(37, 3, firstAtividade+=4, (atividadesColumn + 2), White);
    atividade3Label = firstAtividade + 2;

    int aux2 = 0, index2 = 0;
    int color;
    char icon;

    for(aux2 = limiteInferiorIndexAtividade; aux2 < limiteSuperiorIndexAtividade; aux2++){

        if(atividades->atividades[aux2].status == Entregue){
            icon = 'V';
            color = Green;
        }
        else if(atividades->atividades[aux2].status == Normal){
            icon = '!';
            color = White;
        }
        else if(atividades->atividades[aux2].status == Atencao){
            icon = '!';
            color = Yellow;
        }
        else{
            icon = 'X';
            color = Red;
        }

        if(index2 == 0){
            attron(COLOR_PAIR(color));
            mvprintw(atividade1Label, ((atividadesColumn)+((36 - strlen(atividades->atividades[aux2].data) - strlen(atividades->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividades->atividades[aux2].data, atividades->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        else if(index2 == 1){
            attron(COLOR_PAIR(color));
            mvprintw(atividade2Label, ((atividadesColumn)+((36 - strlen(atividades->atividades[aux2].data) - strlen(atividades->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividades->atividades[aux2].data, atividades->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        else if(index2 == 2){
            attron(COLOR_PAIR(color));
            mvprintw(atividade3Label, ((atividadesColumn)+((36 - strlen(atividades->atividades[aux2].data) - strlen(atividades->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividades->atividades[aux2].data, atividades->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        index2++;
    }

    vertexButtonPreviousPageAtividades = drawBox(10, 4, (Height - 4), (atividadesColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw((Height - 4) + 2, (((atividadesColumn) + 1)+((10 - 1)/2)),"/");
    mvprintw((Height - 4) + 3, (((atividadesColumn) + 1)+((10 - 1)/2)),"\\");
    attroff(COLOR_PAIR(White));

    vertexButtonNextPageAtividades = drawBox(10, 4, (Height - 4), (atividadesColumn + 2 + 29), White);

    attron(COLOR_PAIR(White));
    mvprintw((Height - 4) + 2, (((atividadesColumn + 2 + 29) + 1)+((10 - 1)/2)),"\\");
    mvprintw((Height - 4) + 3, (((atividadesColumn + 2 + 29) + 1)+((10 - 1)/2)),"/");
    attroff(COLOR_PAIR(White));

    //A��es----------------------------------------------------------------------------------------------------------------------------------------

    int actionsLine = headerLine + 3, actionsColumn = headerColumn + 41 + ((Width - (headerColumn + 41) - 25)/2);

    Aluno *currentAluno;

    currentAluno = getAlunoLogado();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine + 1, ((actionsColumn + 1)+((25 - (5 + strlen(currentAluno->nome)))/2)),"Ola %s!",currentAluno->nome);
    attroff(COLOR_PAIR(White));

    vertexButtonCadastrarMateria = drawBox(25, 3, actionsLine, (actionsColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(actionsLine + 2, ((actionsColumn + 1)+((25 - 17)/2)),"Cadastrar Materia");
    attroff(COLOR_PAIR(White));

    actionsLine += 4;

    vertexButtonCadastrarAtividade = drawBox(25, 3, actionsLine, (actionsColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(actionsLine + 2, ((actionsColumn + 1)+((25 - 19)/2)),"Cadastrar Atividade");
    attroff(COLOR_PAIR(White));

    actionsLine += 4;

    vertexButtonEditarMatricula = drawBox(25, 3, actionsLine, (actionsColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(actionsLine + 2, ((actionsColumn + 1)+((25 - 12)/2)),"Trocar Senha");
    attroff(COLOR_PAIR(White));

    actionsLine += 4;

    vertexButtonLogout = drawBox(25, 3, actionsLine, (actionsColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(actionsLine + 2, ((actionsColumn + 1)+((25 - 4)/2)),"Sair");
    attroff(COLOR_PAIR(White));

    actionsLine += 4;


    mainMenuBoxes[0] = vertexButtonMateria1;
    mainMenuBoxes[1] = vertexButtonMateria2;
    mainMenuBoxes[2] = vertexButtonMateria3;
    mainMenuBoxes[3] = vertexButtonNextPageMaterias;
    mainMenuBoxes[4] = vertexButtonPreviousPageMaterias;
    mainMenuBoxes[5] = vertexButtonAtividade;
    mainMenuBoxes[6] = vertexButtonAtividade2;
    mainMenuBoxes[7] = vertexButtonAtividade3;
    mainMenuBoxes[8] = vertexButtonNextPageAtividades;
    mainMenuBoxes[9] = vertexButtonPreviousPageAtividades;
    mainMenuBoxes[10] = vertexButtonCadastrarMateria;
    mainMenuBoxes[11] = vertexButtonCadastrarAtividade;
    mainMenuBoxes[12] = vertexButtonEditarMatricula;
    mainMenuBoxes[13] = vertexButtonLogout;

    return mainMenuBoxes;
}

CoordinatePairs **showCadastrarMateriaScreen(){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **cadastrarMateriaBoxes = (CoordinatePairs **)calloc(3, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexName, *vertexButtonVoltar, *vertexButtonCadastrar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((headerColumn + 1 + 1)+((40 - 15)/2)),"Nome da Materia");
    attroff(COLOR_PAIR(White));

    vertexName = drawBox(40, 2, 15, (headerColumn + 1), White);

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonCadastrar = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Cadastrar");
    attroff(COLOR_PAIR(White));

    cadastrarMateriaBoxes[0] = vertexName;
    cadastrarMateriaBoxes[1] = vertexButtonVoltar;
    cadastrarMateriaBoxes[2] = vertexButtonCadastrar;

    return cadastrarMateriaBoxes;
}

CoordinatePairs **showCadastrarAtividadeScreen(int indexMaterias){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **cadastrarAtividadeBoxes = (CoordinatePairs **)calloc(8, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexName, *vertexDescricao, *vertexValor, *vertexData, *vertexButtonNextPageMaterias, *vertexButtonPreviousPageMaterias, *vertexButtonVoltar, *vertexButtonCadastrar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    int colunaA = (headerColumn - 20);
    int colunaB = (headerColumn + 22);

    attron(COLOR_PAIR(White));
    mvprintw(8, ((colunaA + 1)+((40 - 17)/2)),"Nome da Atividade");
    attroff(COLOR_PAIR(White));

    vertexName = drawBox(40, 2, 9, colunaA, White);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((colunaA + 1)+((40 - 22)/2)),"Descricao da Atividade");
    attroff(COLOR_PAIR(White));

    vertexDescricao = drawBox(40, 2, 15, colunaA, White);

    attron(COLOR_PAIR(White));
    mvprintw(20, ((colunaA + 1)+((40 - 18)/2)),"Valor da Atividade");
    attroff(COLOR_PAIR(White));

    vertexValor = drawBox(40, 2, 21, colunaA, White);

    attron(COLOR_PAIR(White));
    mvprintw(8, ((colunaB + 1)+((40 - 28)/2)),"Data de Entrega da Atividade");
    attroff(COLOR_PAIR(White));

    vertexData = drawBox(40, 2, 9, colunaB, White);

    int materiaLabel = 16;

    attron(COLOR_PAIR(White));
    mvprintw(14, ((colunaB + 1)+((40 - 7)/2)),"Materia");
    attroff(COLOR_PAIR(White));

    drawBox(40, 2, 15, colunaB, White);

    vertexButtonPreviousPageMaterias = drawBox(10, 4, 19, colunaB, White);

    attron(COLOR_PAIR(White));
    mvprintw(19 + 2, ((colunaB + 1)+((10 - 1)/2)),"/");
    mvprintw(19 + 3, ((colunaB + 1)+((10 - 1)/2)),"\\");
    attroff(COLOR_PAIR(White));

    vertexButtonNextPageMaterias = drawBox(10, 4, 19, (colunaB + 30), White);

    attron(COLOR_PAIR(White));
    mvprintw(19 + 2, (((colunaB + 30) + 1)+((10 - 1)/2)),"\\");
    mvprintw(19 + 3, (((colunaB + 30) + 1)+((10 - 1)/2)),"/");
    attroff(COLOR_PAIR(White));

    ArrayMaterias *materias;

    materias = getMateriasOfCurrentUser();

    if(indexMaterias > materias->size){
        indexMaterias = materias->size;
    }

    if(indexMaterias < 0){
        indexMaterias = 0;
    }

    attron(COLOR_PAIR(White));
    mvprintw(materiaLabel, ((colunaB + 1)+((40 - strlen(materias->materias[indexMaterias].nome))/2)),"%s",materias->materias[indexMaterias].nome);
    attroff(COLOR_PAIR(White));

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonCadastrar = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Cadastrar");
    attroff(COLOR_PAIR(White));

    cadastrarAtividadeBoxes[0] = vertexName;
    cadastrarAtividadeBoxes[1] = vertexDescricao;
    cadastrarAtividadeBoxes[2] = vertexValor;
    cadastrarAtividadeBoxes[3] = vertexData;
    cadastrarAtividadeBoxes[4] = vertexButtonNextPageMaterias;
    cadastrarAtividadeBoxes[5] = vertexButtonPreviousPageMaterias;
    cadastrarAtividadeBoxes[6] = vertexButtonVoltar;
    cadastrarAtividadeBoxes[7] = vertexButtonCadastrar;

    return cadastrarAtividadeBoxes;
}

CoordinatePairs **showDetalhesMateriaScreen(int indexMateria){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **detalhesMateriaBoxes = (CoordinatePairs **)calloc(4, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexError, *vertexButtonVoltar, *vertexButtonTrancar, *vertexButtonListarAtividades, *vertexButtonEditar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    ArrayMaterias *materias;

    materias = getMateriasOfCurrentUser();

    if(indexMateria >= materias->size){
        vertexError = calloc(1, sizeof(CoordinatePairs));
        vertexError->column = 0;
        vertexError->line = 0;
        detalhesMateriaBoxes[0] = vertexError;
        return detalhesMateriaBoxes;
    }
    else{
        attron(COLOR_PAIR(White));
        mvprintw(14, ((headerColumn + 1 + 1)+((40 - 15)/2)),"Nome da Materia");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 15, (headerColumn + 1), White);

        attron(COLOR_PAIR(White));
        mvprintw(16, ((headerColumn + 1 + 1)+((40 - strlen(materias->materias[indexMateria].nome))/2)),"%s", materias->materias[indexMateria].nome);
        attroff(COLOR_PAIR(White));

        vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn - 22), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn - 22)+((20 - 5)/2)),"Voltar");
        attroff(COLOR_PAIR(White));

        vertexButtonTrancar = drawBox(20, 3, 25, (headerColumn), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 1)+((20 - 7)/2)),"Trancar");
        attroff(COLOR_PAIR(White));

        vertexButtonListarAtividades = drawBox(20, 3, 25, (headerColumn + 22), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 22 + 1)+((20 - 17)/2)),"Listar Atividades");
        attroff(COLOR_PAIR(White));

        vertexButtonEditar = drawBox(20, 3, 25, (headerColumn + 44), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 44 + 1)+((20 - 6)/2)),"Editar");
        attroff(COLOR_PAIR(White));

        detalhesMateriaBoxes[0] = vertexButtonVoltar;
        detalhesMateriaBoxes[1] = vertexButtonTrancar;
        detalhesMateriaBoxes[2] = vertexButtonListarAtividades;
        detalhesMateriaBoxes[3] = vertexButtonEditar;

        return detalhesMateriaBoxes;
    }
}

CoordinatePairs **showEditarMateriaScreen(int indexMateria){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **editarMateriaBoxes = (CoordinatePairs **)calloc(3, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexName, *vertexButtonVoltar, *vertexButtonAtualizar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    ArrayMaterias *materias;

    materias = getMateriasOfCurrentUser();

    attron(COLOR_PAIR(White));
    mvprintw(14, ((headerColumn + 1 + 1)+((40 - 15)/2)),"Nome da Materia");
    attroff(COLOR_PAIR(White));

    vertexName = drawBox(40, 2, 15, (headerColumn + 1), White);

    attron(COLOR_PAIR(White));
    mvprintw(16, (headerColumn + 1 + 1),"%s", materias->materias[indexMateria].nome);
    attroff(COLOR_PAIR(White));

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonAtualizar = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Atualizar");
    attroff(COLOR_PAIR(White));

    editarMateriaBoxes[0] = vertexName;
    editarMateriaBoxes[1] = vertexButtonVoltar;
    editarMateriaBoxes[2] = vertexButtonAtualizar;

    return editarMateriaBoxes;
}

CoordinatePairs **showListarAtividadesScreen(int indexMateria, int paginacaoAtividade){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **listarAtividadesBoxes = (CoordinatePairs **)calloc(3, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexError, *vertexButtonVoltar, *vertexButtonPreviousPageAtividades, *vertexButtonNextPageAtividades;;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    ArrayMaterias *materias;
    materias = getMateriasOfCurrentUser();

    ArrayAtividades *atividades, *atividadesMateria;

    atividades = getArrayAtividades();

    atividadesMateria = getAtividadesOfCurrentUserandMateria(materias->materias[indexMateria].id);

    int newSize = atividadesMateria->size;

    if(newSize == 0){
        vertexError = calloc(1, sizeof(CoordinatePairs));
        vertexError->column = 0;
        vertexError->line = 0;
        listarAtividadesBoxes[0] = vertexError;
        return listarAtividadesBoxes;
    }

    atividadesMateria->atividades = calloc(newSize, sizeof(Atividade));

    int aux, newIndex = 0;

    for(aux=0; aux < atividades->size; aux++){
        if(atividades->atividades[aux].materiaId == materias->materias[indexMateria].id){
            atividadesMateria->atividades[newIndex] = atividades->atividades[aux];
            newIndex++;
        }
    }


    int limiteSuperiorIndexAtividade = paginacaoAtividade * 3;
    int limiteInferiorIndexAtividade = limiteSuperiorIndexAtividade - 3;

    if(limiteSuperiorIndexAtividade > atividadesMateria->size){
        limiteSuperiorIndexAtividade = atividadesMateria->size;
    }

    if(limiteInferiorIndexAtividade < 0){
        limiteInferiorIndexAtividade = 0;
    }

    int firstAtividade = headerLine;
    int atividade1Label, atividade2Label, atividade3Label;

    drawBox(37, 3, firstAtividade, (headerColumn + 2), White);
    atividade1Label = firstAtividade + 2;

    drawBox(37, 3, firstAtividade+=4, (headerColumn + 2), White);
    atividade2Label = firstAtividade + 2;

    drawBox(37, 3, firstAtividade+=4, (headerColumn + 2), White);
    atividade3Label = firstAtividade + 2;

    int aux2 = 0, index2 = 0;
    int color;
    char icon;

    for(aux2 = limiteInferiorIndexAtividade; aux2 < limiteSuperiorIndexAtividade; aux2++){

        if(atividadesMateria->atividades[aux2].status == Entregue){
            icon = 'V';
            color = Green;
        }
        else if(atividadesMateria->atividades[aux2].status == Normal){
            icon = '!';
            color = White;
        }
        else if(atividadesMateria->atividades[aux2].status == Atencao){
            icon = '!';
            color = Yellow;
        }
        else{
            icon = 'X';
            color = Red;
        }

        if(index2 == 0){
            attron(COLOR_PAIR(color));
            mvprintw(atividade1Label, ((headerColumn)+((36 - strlen(atividadesMateria->atividades[aux2].data) - strlen(atividadesMateria->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividadesMateria->atividades[aux2].data, atividadesMateria->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        else if(index2 == 1){
            attron(COLOR_PAIR(color));
            mvprintw(atividade2Label, ((headerColumn)+((36 - strlen(atividadesMateria->atividades[aux2].data) - strlen(atividadesMateria->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividadesMateria->atividades[aux2].data, atividadesMateria->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        else if(index2 == 2){
            attron(COLOR_PAIR(color));
            mvprintw(atividade3Label, ((headerColumn)+((36 - strlen(atividadesMateria->atividades[aux2].data) - strlen(atividadesMateria->atividades[aux2].nome) )/2)),"(%c) %s - %s", icon, atividadesMateria->atividades[aux2].data, atividadesMateria->atividades[aux2].nome);
            attroff(COLOR_PAIR(color));
        }
        index2++;
    }

    vertexButtonPreviousPageAtividades = drawBox(10, 4, firstAtividade+=4, (headerColumn + 2), White);

    attron(COLOR_PAIR(White));
    mvprintw(firstAtividade + 2, (((headerColumn + 2) + 1)+((10 - 1)/2)),"/");
    mvprintw(firstAtividade + 3, (((headerColumn + 2) + 1)+((10 - 1)/2)),"\\");
    attroff(COLOR_PAIR(White));

    vertexButtonNextPageAtividades = drawBox(10, 4, firstAtividade, (headerColumn + 29), White);

    attron(COLOR_PAIR(White));
    mvprintw(firstAtividade + 2, (((headerColumn + 29) + 1)+((10 - 1)/2)),"\\");
    mvprintw(firstAtividade + 3, (((headerColumn + 29) + 1)+((10 - 1)/2)),"/");
    attroff(COLOR_PAIR(White));

    vertexButtonVoltar = drawBox(19, 3, 25, (headerColumn + 11), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 11 + 1)+((19 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    listarAtividadesBoxes[0] = vertexButtonVoltar;
    listarAtividadesBoxes[1] = vertexButtonPreviousPageAtividades;
    listarAtividadesBoxes[2] = vertexButtonNextPageAtividades;

    return listarAtividadesBoxes;
}

CoordinatePairs **showDetalhesAtividadeScreen(int indexAtividades){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **detalhesAtividadeBoxes = (CoordinatePairs **)calloc(4, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexError, *vertexButtonEntregar, *vertexButtonRemover, *vertexButtonVoltar, *vertexButtonEditar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    ArrayAtividades *atividades;

    atividades = getArrayAtividades();


    if(indexAtividades >= atividades->size){
        vertexError = calloc(1, sizeof(CoordinatePairs));
        vertexError->column = 0;
        vertexError->line = 0;
        detalhesAtividadeBoxes[0] = vertexError;
        return detalhesAtividadeBoxes;
    }
    else{
        int colunaA = (headerColumn - 20);
        int colunaB = (headerColumn + 22);

        attron(COLOR_PAIR(White));
        mvprintw(8, ((colunaA + 1)+((40 - 17)/2)),"Nome da Atividade");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 9, colunaA, White);

        attron(COLOR_PAIR(White));
        mvprintw(10, ((colunaA + 1 + 1)+((40 - strlen(atividades->atividades[indexAtividades].nome))/2)),"%s", atividades->atividades[indexAtividades].nome);
        attroff(COLOR_PAIR(White));

        attron(COLOR_PAIR(White));
        mvprintw(14, ((colunaA + 1)+((40 - 22)/2)),"Descricao da Atividade");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 15, colunaA, White);

        attron(COLOR_PAIR(White));
        mvprintw(16, ((colunaA + 1 + 1)+((40 - strlen(atividades->atividades[indexAtividades].descricao))/2)),"%s", atividades->atividades[indexAtividades].descricao);
        attroff(COLOR_PAIR(White));

        attron(COLOR_PAIR(White));
        mvprintw(20, ((colunaA + 1)+((40 - 18)/2)),"Valor da Atividade");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 21, colunaA, White);

        attron(COLOR_PAIR(White));
        mvprintw(22, ((colunaA + 1 + 1)+((40 - strlen(atividades->atividades[indexAtividades].valor))/2)),"%s", atividades->atividades[indexAtividades].valor);
        attroff(COLOR_PAIR(White));

        attron(COLOR_PAIR(White));
        mvprintw(8, ((colunaB + 1)+((40 - 28)/2)),"Data de Entrega da Atividade");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 9, colunaB, White);

        attron(COLOR_PAIR(White));
        mvprintw(10, ((colunaB + 1 + 1)+((40 - strlen(atividades->atividades[indexAtividades].data))/2)),"%s", atividades->atividades[indexAtividades].data);
        attroff(COLOR_PAIR(White));

        int materiaLabel = 16;

        attron(COLOR_PAIR(White));
        mvprintw(14, ((colunaB + 1)+((40 - 7)/2)),"Materia");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 15, colunaB, White);

        ArrayMaterias *materias;

        materias = getMateriasOfCurrentUser();

        int aux, indexMaterias = -1;

        for(aux = 0; aux < materias->size; aux++){
            if(materias->materias[aux].id == atividades->atividades[indexAtividades].materiaId){
                indexMaterias = aux;
            }
        }

        if(indexMaterias == -1){
            vertexError = calloc(1, sizeof(CoordinatePairs));
            vertexError->column = 1;
            vertexError->line = 1;
            detalhesAtividadeBoxes[0] = vertexError;
            return detalhesAtividadeBoxes;
        }

        attron(COLOR_PAIR(White));
        mvprintw(materiaLabel, ((colunaB + 1)+((40 - strlen(materias->materias[indexMaterias].nome))/2)),"%s",materias->materias[indexMaterias].nome);
        attroff(COLOR_PAIR(White));


        attron(COLOR_PAIR(White));
        mvprintw(20, ((colunaB + 1)+((40 - 19)/2)),"Status da Atividade");
        attroff(COLOR_PAIR(White));

        drawBox(40, 2, 21, colunaB, White);

        char *status, color;

        if(atividades->atividades[indexAtividades].status == Normal){
            status = calloc(7, sizeof(char));
            strcat(status, "Normal");
            color = White;
        }
        else if(atividades->atividades[indexAtividades].status == Entregue){
            status = calloc(9, sizeof(char));
            strcat(status, "Entregue");
            color = Green;
        }
        else if(atividades->atividades[indexAtividades].status == Atencao){
            status = calloc(8, sizeof(char));
            strcat(status, "Atencao");
            color = Yellow;
        }
        else{
            status = calloc(9, sizeof(char));
            strcat(status, "Atrasada");
            color = Red;
        }

        attron(COLOR_PAIR(color));
        mvprintw(22, ((colunaB + 1 + 1)+((40 - strlen(status))/2)),"%s", status);
        attroff(COLOR_PAIR(color));


        vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn - 22), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn - 22)+((20 - 5)/2)),"Voltar");
        attroff(COLOR_PAIR(White));

        vertexButtonRemover = drawBox(20, 3, 25, (headerColumn), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 1)+((20 - 17)/2)),"Remover Atividade");
        attroff(COLOR_PAIR(White));

        vertexButtonEntregar = drawBox(20, 3, 25, (headerColumn + 22), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 22 + 1)+((20 - 18)/2)),"Entregar Atividade");
        attroff(COLOR_PAIR(White));

        vertexButtonEditar = drawBox(20, 3, 25, (headerColumn + 44), White);

        attron(COLOR_PAIR(White));
        mvprintw(27, ((headerColumn + 44 + 1)+((20 - 6)/2)),"Editar");
        attroff(COLOR_PAIR(White));

        detalhesAtividadeBoxes[0] = vertexButtonVoltar;
        detalhesAtividadeBoxes[1] = vertexButtonRemover;
        detalhesAtividadeBoxes[2] = vertexButtonEntregar;
        detalhesAtividadeBoxes[3] = vertexButtonEditar;

        return detalhesAtividadeBoxes;
    }
}

CoordinatePairs **showEditarAtividadeScreen(int indexAtividade){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **editarAtividadeBoxes = (CoordinatePairs **)calloc(6, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexError, *vertexName, *vertexDescricao, *vertexValor, *vertexData, *vertexButtonVoltar, *vertexButtonAtualizar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    ArrayAtividades *atividades;
    atividades = getArrayAtividades();

    if(atividades->atividades[indexAtividade].status == Atrasada){
        vertexError = calloc(1, sizeof(CoordinatePairs));
        vertexError->column = 0;
        vertexError->line = 0;
        editarAtividadeBoxes[0] = vertexError;
        return editarAtividadeBoxes;
    }
    else if(atividades->atividades[indexAtividade].status == Entregue){
        vertexError = calloc(1, sizeof(CoordinatePairs));
        vertexError->column = 1;
        vertexError->line = 1;
        editarAtividadeBoxes[0] = vertexError;
        return editarAtividadeBoxes;
    }

    int colunaA = (headerColumn - 20);
    int colunaB = (headerColumn + 22);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((colunaA + 1)+((40 - 17)/2)),"Nome da Atividade");
    attroff(COLOR_PAIR(White));

    vertexName = drawBox(40, 2, 15, colunaA, White);

    attron(COLOR_PAIR(White));
    mvprintw(16, (colunaA + 1),"%s", atividades->atividades[indexAtividade].nome);
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(20, ((colunaA + 1)+((40 - 22)/2)),"Descricao da Atividade");
    attroff(COLOR_PAIR(White));

    vertexDescricao = drawBox(40, 2, 21, colunaA, White);

    attron(COLOR_PAIR(White));
    mvprintw(22, (colunaA + 1),"%s", atividades->atividades[indexAtividade].descricao);
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(14, ((colunaB + 1)+((40 - 18)/2)),"Valor da Atividade");
    attroff(COLOR_PAIR(White));

    vertexValor = drawBox(40, 2, 15, colunaB, White);

    attron(COLOR_PAIR(White));
    mvprintw(16, (colunaB + 1),"%s", atividades->atividades[indexAtividade].valor);
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(20, ((colunaB + 1)+((40 - 28)/2)),"Data de Entrega da Atividade");
    attroff(COLOR_PAIR(White));

    vertexData = drawBox(40, 2, 21, colunaB, White);

    attron(COLOR_PAIR(White));
    mvprintw(22, (colunaB + 1),"%s", atividades->atividades[indexAtividade].data);
    attroff(COLOR_PAIR(White));

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonAtualizar = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Atualizar");
    attroff(COLOR_PAIR(White));

    editarAtividadeBoxes[0] = vertexName;
    editarAtividadeBoxes[1] = vertexDescricao;
    editarAtividadeBoxes[2] = vertexValor;
    editarAtividadeBoxes[3] = vertexData;
    editarAtividadeBoxes[4] = vertexButtonVoltar;
    editarAtividadeBoxes[5] = vertexButtonAtualizar;

    return editarAtividadeBoxes;
}

CoordinatePairs **showChangePasswordScreen(){
    int headerLine = 0, headerColumn = (Width - 41)/2;
    CoordinatePairs **changePasswordBoxes = (CoordinatePairs **)calloc(3, sizeof(CoordinatePairs *));
    CoordinatePairs *vertexPasssword, *vertexButtonVoltar, *vertexButtonAtualizar;

    clear();

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"   _____  ___________________    _________");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"  /  _  \\ \\_   _____/\\______ \\  /   _____/");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn," /  /_\\  \\ |    __)_  |    |  \\ \\_____  \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"/    |    \\|        \\ |    `   \\/        \\");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"\\____|__  /_______  //_______  /_______  /");
    attroff(COLOR_PAIR(White));

    attron(COLOR_PAIR(White));
    mvprintw(headerLine++, headerColumn,"        \\/        \\/         \\/        \\/");
    attroff(COLOR_PAIR(White));

    drawLine(0, Width, headerLine++, White);

    attron(COLOR_PAIR(White));
    mvprintw(14, ((headerColumn + 1 + 1)+((40 - 10)/2)),"Nova senha");
    attroff(COLOR_PAIR(White));

    vertexPasssword = drawBox(40, 2, 15, (headerColumn + 1), White);

    vertexButtonVoltar = drawBox(20, 3, 25, (headerColumn), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 1)+((20 - 5)/2)),"Voltar");
    attroff(COLOR_PAIR(White));

    vertexButtonAtualizar = drawBox(20, 3, 25, (headerColumn + 22), White);

    attron(COLOR_PAIR(White));
    mvprintw(27, ((headerColumn + 22 + 1)+((20 - 9)/2)),"Atualizar");
    attroff(COLOR_PAIR(White));

    changePasswordBoxes[0] = vertexPasssword;
    changePasswordBoxes[1] = vertexButtonVoltar;
    changePasswordBoxes[2] = vertexButtonAtualizar;

    return changePasswordBoxes;
}


void trancarMateria(int indexMateria){
    ArrayMaterias *materias;

    materias = getMateriasOfCurrentUser();

    ArrayMaterias *newArrayMaterias;

    int newSize = materias->size - 1;

    if(newSize == 0){
        newSize = 1;
    }

    newArrayMaterias = calloc(1, sizeof(ArrayMaterias));
    newArrayMaterias->materias = calloc(newSize, sizeof(Materia));
    newArrayMaterias->used = newSize;
    newArrayMaterias->size = newSize;

    int aux, newIndex = 0;

    for(aux = 0; aux < materias->size; aux++){
        if(aux != indexMateria){
           newArrayMaterias->materias[newIndex++] =  materias->materias[aux];
        }
    }

    updateMaterias(newArrayMaterias);

    attron(COLOR_PAIR(Success));
    mvprintw( Height, (Width - 28)/2 + 1 , "Materia trancada com sucesso");
    attroff(COLOR_PAIR(Success));
    free(materias);
    free(newArrayMaterias);
}

int editarMateria(int indexMateria, char* name, CoordinatePairs *nameField){
    int validFields = TRUE;

    if(strlen(name) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(nameField[2].line + 1, nameField[0].column + (((nameField[1].column - nameField[0].column) - 10)/2), "Nome invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }

    if(validFields == TRUE){

        ArrayMaterias *materias;

        materias = getMateriasOfCurrentUser();

        ArrayMaterias *newArrayMaterias;

        int newSize = materias->size;

        newArrayMaterias = calloc(1, sizeof(ArrayMaterias));
        newArrayMaterias->materias = calloc(newSize, sizeof(Materia));
        newArrayMaterias->used = newSize;
        newArrayMaterias->size = newSize;

        int aux;
        for(aux = 0; aux < materias->size; aux++){
            if(aux != indexMateria){
               newArrayMaterias->materias[aux] =  materias->materias[aux];
            }
            else{
                newArrayMaterias->materias[aux] =  materias->materias[aux];
                newArrayMaterias->materias[aux].nome = calloc(strlen(name),sizeof(char));
                strcat(newArrayMaterias->materias[aux].nome, name);
            }
        }
        updateMaterias(newArrayMaterias);

        attron(COLOR_PAIR(Success));
        mvprintw( Height, (Width - 30)/2 + 1 , "Materia atualizada com sucesso");
        attroff(COLOR_PAIR(Success));

        return TRUE;
    }
    else{
        return FALSE;
    }
}

void removerAtividade(int indexAtividade, int hideMessage){
    ArrayAtividades *atividades;

    atividades = getArrayAtividades();

    ArrayAtividades *newArrayAtividades;

    int newSize = atividades->size - 1;

    if(newSize == 0){
        newSize = 1;
    }

    newArrayAtividades = calloc(1, sizeof(ArrayAtividades));
    newArrayAtividades->atividades = calloc(newSize, sizeof(Atividade));
    newArrayAtividades->used = newSize;
    newArrayAtividades->size = newSize;

    int aux, newIndex = 0;

    for(aux = 0; aux < atividades->size; aux++){
        if(aux != indexAtividade){
           newArrayAtividades->atividades[newIndex] =  atividades->atividades[aux];
           if(newArrayAtividades->atividades[newIndex].status != Entregue){
                newArrayAtividades->atividades[newIndex].status = Pendente;
           }
           newIndex++;
        }
    }

    updateAtividades(newArrayAtividades);

    if(hideMessage == FALSE){
        attron(COLOR_PAIR(Success));
        mvprintw( Height, (Width - 30)/2 + 1 , "Atividade removida com sucesso");
        attroff(COLOR_PAIR(Success));
        free(atividades);
        free(newArrayAtividades);
    }
}

int entregarAtividade(int indexAtividade){
    ArrayAtividades *atividades;

    atividades = getArrayAtividades();

    if(atividades->atividades[indexAtividade].status == Atrasada){
        return MotivoAtraso;
    }
    else if(atividades->atividades[indexAtividade].status == Entregue){
        return MotivoEntregue;
    }


    ArrayAtividades *newArrayAtividades;

    int newSize = atividades->size;

    newArrayAtividades = calloc(1, sizeof(ArrayAtividades));
    newArrayAtividades->atividades = calloc(newSize, sizeof(Atividade));
    newArrayAtividades->used = newSize;
    newArrayAtividades->size = newSize;

    int aux;

    for(aux = 0; aux < atividades->size; aux++){
        if(aux != indexAtividade){
           newArrayAtividades->atividades[aux] =  atividades->atividades[aux];
           if(newArrayAtividades->atividades[aux].status != Entregue){
                newArrayAtividades->atividades[aux].status = Pendente;
           }
        }
        else{
            newArrayAtividades->atividades[aux] =  atividades->atividades[aux];
            newArrayAtividades->atividades[aux].status = Entregue;
        }
    }

    updateAtividades(newArrayAtividades);

    attron(COLOR_PAIR(Success));
    mvprintw( Height, (Width - 30)/2 + 1 , "Atividade entregue com sucesso");
    attroff(COLOR_PAIR(Success));
    free(atividades);
    free(newArrayAtividades);

    return Sucesso;
}

int editarAtividade(int indexAtividade, char* name, char* descricao, char* valor, char* data, CoordinatePairs *nameField, CoordinatePairs *descricaoField, CoordinatePairs *valorField, CoordinatePairs *dataField){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int validFields = TRUE;

    if(strlen(name) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(nameField[2].line + 1, nameField[0].column + (((nameField[1].column - nameField[0].column) - 10)/2), "Nome invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(descricao) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(descricaoField[2].line + 1, descricaoField[0].column + (((descricaoField[1].column - descricaoField[0].column) - 18)/2), "Descricao invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(valor) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(valorField[2].line + 1, valorField[0].column + (((valorField[1].column - valorField[0].column) - 11)/2), "Valor invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    else if(atof(valor) == 0){
        attron(COLOR_PAIR(Danger));
        mvprintw(valorField[2].line + 1, valorField[0].column + (((valorField[1].column - valorField[0].column) - 11)/2), "Valor invalido");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    if(strlen(data) < 10){
        attron(COLOR_PAIR(Danger));
        mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }
    else{
        char dia[3], mes[3], ano[5];

        dia[0] = data[0];
        dia[1] = data[1];
        dia[2] = '\0';
        mes[0] = data[3];
        mes[1] = data[4];
        mes[2] = '\0';
        ano[0] = data[6];
        ano[1] = data[7];
        ano[2] = data[8];
        ano[3] = data[9];
        ano[4] = '\0';


        if(atoll(dia) > 0 && atoll(mes) > 0 && atoll(ano) > 0){
            if(atoll(dia) > 31){
                attron(COLOR_PAIR(Danger));
                mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
                attroff(COLOR_PAIR(Danger));
                refresh();
                validFields = FALSE;
            }
            else if(atoll(mes) > 12){
                attron(COLOR_PAIR(Danger));
                mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
                attroff(COLOR_PAIR(Danger));
                refresh();
                validFields = FALSE;
            }
            else{
                if(atoll(ano) < tm.tm_year + 1900){
                    attron(COLOR_PAIR(Danger));
                    mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Ano invalido");
                    attroff(COLOR_PAIR(Danger));
                    refresh();
                    validFields = FALSE;
                }
                else{
                    if(atoll(ano) <= tm.tm_year + 1900){
                        if(atoll(mes) < tm.tm_mon + 1){
                            attron(COLOR_PAIR(Danger));
                            mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Mes invalido");
                            attroff(COLOR_PAIR(Danger));
                            refresh();
                            validFields = FALSE;
                        }
                        else{
                            if(atoll(mes) <= tm.tm_mon + 1){
                                if(atoll(dia) < tm.tm_mday){
                                    attron(COLOR_PAIR(Danger));
                                    mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 9)/2), "Dia invalido");
                                    attroff(COLOR_PAIR(Danger));
                                    refresh();
                                    validFields = FALSE;
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            attron(COLOR_PAIR(Danger));
            mvprintw(dataField[2].line + 1, dataField[0].column + (((dataField[1].column - dataField[0].column) - 10)/2), "Data invalida");
            attroff(COLOR_PAIR(Danger));
            refresh();
            validFields = FALSE;
        }


    }

    if(validFields == TRUE){
        ArrayAtividades *atividades;

        atividades = getArrayAtividades();

        ArrayAtividades *newArrayAtividades;

        int newSize = atividades->size;

        newArrayAtividades = calloc(1, sizeof(ArrayAtividades));
        newArrayAtividades->atividades = calloc(newSize, sizeof(Atividade));
        newArrayAtividades->used = newSize;
        newArrayAtividades->size = newSize;

        int aux, oldStatus;
        for(aux = 0; aux < atividades->size; aux++){
            if(aux != indexAtividade){
               newArrayAtividades->atividades[aux] =  atividades->atividades[aux];
               if(newArrayAtividades->atividades[aux].status != Entregue){
                    newArrayAtividades->atividades[aux].status                 = Pendente;
               }
            }
            else{
                newArrayAtividades->atividades[aux] =  atividades->atividades[aux];

                newArrayAtividades->atividades[aux].nome = calloc(strlen(name),sizeof(char));
                newArrayAtividades->atividades[aux].descricao = calloc(strlen(descricao),sizeof(char));
                newArrayAtividades->atividades[aux].valor = calloc(strlen(valor),sizeof(char));
                newArrayAtividades->atividades[aux].data = calloc(strlen(data),sizeof(char));

                oldStatus = newArrayAtividades->atividades[aux].status;
                newArrayAtividades->atividades[aux].status = Pendente;

                strcat(newArrayAtividades->atividades[aux].nome, name);
                strcat(newArrayAtividades->atividades[aux].descricao, descricao);
                strcat(newArrayAtividades->atividades[aux].valor, valor);
                strcat(newArrayAtividades->atividades[aux].data, data);
            }
        }
        updateAtividades(newArrayAtividades);

        newArrayAtividades->atividades[indexAtividade].status = oldStatus;
        setArrayAtividades(newArrayAtividades);

        attron(COLOR_PAIR(Success));
        mvprintw( Height, (Width - 32)/2 + 1 , "Atividade atualizada com sucesso");
        attroff(COLOR_PAIR(Success));

        return TRUE;

    }
    else{
        return FALSE;
    }
}

int changePassword(char* password, CoordinatePairs *passwordField){
    int validFields = TRUE;

    if(strlen(password) == 0 || strlen(password) < 8){
        attron(COLOR_PAIR(Danger));
        mvprintw(passwordField[2].line + 1, passwordField[0].column + (((passwordField[1].column - passwordField[0].column) - 12)/2), "Senha invalida");
        attroff(COLOR_PAIR(Danger));
        refresh();
        validFields = FALSE;
    }

    if(validFields == TRUE){
        updateAluno(password);

        attron(COLOR_PAIR(Success));
        mvprintw( Height, (Width - 28)/2 + 1 , "Senha atualizada com sucesso");
        attroff(COLOR_PAIR(Success));

        return TRUE;
    }
    else{
        return FALSE;
    }
}

void showErrorMessage(int message){
    if(message == 1){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 26)/2 + 1 , "Nenhuma materia cadastrada");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 2){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 21)/2 + 1 , "Materia nao encontrada");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 3){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 51)/2 + 1 , "Nao existem atividades cadastradas para esta tarefa");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 4){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 24)/2 + 1 , "Atividade nao encontrada");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 5){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 48)/2 + 1 , "Materia nao encontrada, Atividade sera removida");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 6){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 57)/2 + 1 , "Uma Atividade com o status ATRASADA nao pode ser alterada");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
    else if(message == 7){
        attron(COLOR_PAIR(Danger));
        mvprintw( Height, (Width - 57)/2 + 1 , "Uma Atividade com o status ENTREGUE nao pode ser alterada");
        attroff(COLOR_PAIR(Danger));
        refresh();
    }
}

char *getMateriaName(int indexMateria){
    ArrayMaterias *materias;
    materias = getMateriasOfCurrentUser();

    return materias->materias[indexMateria].nome;
}

char *getAtividadeProperty(int indexAtividade, int property){
    ArrayAtividades *atividades;
    atividades = getArrayAtividades();

    if(property == Nome){
        return atividades->atividades[indexAtividade].nome;
    }
    else if(property == Descricao){
        return atividades->atividades[indexAtividade].descricao;
    }
    else if(property == Valor){
       return atividades->atividades[indexAtividade].valor;
    }
    else if(property == Data){
        return atividades->atividades[indexAtividade].data;
    }
}

void autoSetField(char* currentValue, CoordinatePairs *vertexBox){
    attron(COLOR_PAIR(White));
    mvprintw((vertexBox->line + 1), (vertexBox->column + 1),"%s", currentValue);
    attroff(COLOR_PAIR(White));
}

#endif // GUI_H_INCLUDED
