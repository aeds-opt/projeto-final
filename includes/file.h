#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#define Id 0
#define NumeroMatricula 1
#define Senha 2
#define Nome 3

#define IdMateria 0
#define IdUsuario 1
#define NomeMateria 2

#define IdAtividade 0
#define IdUsuarioAtividade 1
#define IdMateriaAtividade 2
#define NomeAtividade 3
#define DescricaoAtividade 4
#define ValorAtividade 5
#define DataAtividade 6
#define MatertiaAtividade 7
#define StatusAtividade 8

#define SizeNumeroMatricula 12
#define SizeSenha 12
#define SizeNome 20
#define SizeDescricao 30
#define SizeValor 6
#define SizeData 10

#define NotFound 0

typedef struct {
    unsigned int id;
    char *numMatricula;
    char *senha;
    char *nome;
}Aluno;

typedef struct {
    Aluno *alunos;
    size_t used;
    size_t size;
}ArrayAlunos;

typedef struct {
    unsigned int id;
    unsigned int userId;
    char *nome;
}Materia;

typedef struct {
    Materia *materias;
    size_t used;
    size_t size;
}ArrayMaterias;

typedef struct {
    unsigned int id;
    unsigned int userId;
    unsigned int materiaId;
    unsigned int status;
    char *nome;
    char *descricao;
    char *valor;
    char *data;
    char *materia;
}Atividade;

typedef struct {
    Atividade *atividades;
    size_t used;
    size_t size;
}ArrayAtividades;

Aluno *getAlunoFromFile(char *login, char *password){
    FILE *alunosTable;
    Aluno *foundedAluno = calloc(1, sizeof(Aluno));
    int ch, property = Id, line = 0, currentSize = 0;
    int sizeId = 1, sizeMatricula = SizeNumeroMatricula, sizePassword = SizeSenha, sizeName = SizeNome;
    char *id = calloc(sizeId,sizeof(char));

    foundedAluno->numMatricula = calloc(sizeMatricula, sizeof(char));
    foundedAluno->senha = calloc(sizePassword, sizeof(char));
    foundedAluno->nome = calloc(sizeName, sizeof(char));

    if((alunosTable = fopen("./tabelas/Alunos.txt", "a+")) == NULL) {
        free(id);
        return foundedAluno;
    }

    while(EOF!=(ch=fgetc(alunosTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == NumeroMatricula){
                    foundedAluno->numMatricula[currentSize++] = (char)ch;
                    if(currentSize == sizeMatricula){
                        sizeMatricula *= 2;
                        foundedAluno->numMatricula = realloc(foundedAluno->numMatricula, sizeMatricula * sizeof(char));
                    }
                }
                else if(property == Senha){
                    foundedAluno->senha[currentSize++] = (char)ch;
                    if(currentSize == sizePassword){
                        sizePassword *= 2;
                        foundedAluno->senha = realloc(foundedAluno->senha, sizePassword * sizeof(char));
                    }
                }
                else if(property == Nome){
                    foundedAluno->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAluno->nome = realloc(foundedAluno->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == Id){
                    id[currentSize++] = (char)ch;
                    foundedAluno->id = atoi(id);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        id = realloc(id, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = Id;
            currentSize = 0;
            if(strcmp(login, foundedAluno->numMatricula) == 0){
                break;
            }
            else{
                sizeId = 1;
                sizeMatricula = SizeNumeroMatricula;
                sizePassword = SizeSenha;
                sizeName = SizeNome;
                foundedAluno->id = 0;
                foundedAluno->numMatricula = calloc(sizeMatricula, sizeof(char));
                foundedAluno->senha = calloc(sizePassword, sizeof(char));
                foundedAluno->nome = calloc(sizeName, sizeof(char));
            }
        }
    }

    if(line == 0){
        fprintf(alunosTable,"%s,%s,%s,%s\n","ID","MATRICULA","SENHA","NOME");
    }

    if(alunosTable) {
        fclose(alunosTable);
    }

    free(id);

    return foundedAluno;
}

Aluno *postAlunoOnFile(char *login, char *password, char *Name){
    FILE *alunosTable;
    Aluno *newAluno = calloc(1, sizeof(Aluno));
    int ch, property = Id, line = 0, currentSize = 0;
    int sizeId = 1, sizeMatricula = SizeNumeroMatricula;
    char *id = calloc(sizeId,sizeof(char)), *numeroMatricula = calloc(sizeMatricula, sizeof(char));

    newAluno->numMatricula = calloc(strlen(login), sizeof(char));
    newAluno->senha = calloc(strlen(password), sizeof(char));
    newAluno->nome = calloc(strlen(Name), sizeof(char));

    strcat(newAluno->numMatricula, login);
    strcat(newAluno->senha, password);
    strcat(newAluno->nome, Name);

    if((alunosTable = fopen("./tabelas/Alunos.txt", "a+")) == NULL) {
        free(id);
        free(numeroMatricula);
        return newAluno;
    }

    while(EOF!=(ch=fgetc(alunosTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == Id){
                    id[currentSize++] = (char)ch;
                    newAluno->id = atoi(id);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        id = realloc(id, sizeId * sizeof(char));
                    }
                }
                if(property == NumeroMatricula){
                    numeroMatricula[currentSize++] = (char)ch;
                    if(currentSize == sizeMatricula){
                        sizeMatricula *= 2;
                        numeroMatricula = realloc(numeroMatricula, sizeMatricula * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = Id;
            currentSize = 0;
            sizeId = 1;
            sizeMatricula = SizeNumeroMatricula;
            if(strcmp(newAluno->numMatricula, numeroMatricula) == 0){
                newAluno->numMatricula = "0";
                break;
            }
        }
    }

    if(newAluno->numMatricula == "0"){
        if(alunosTable) {
            fclose(alunosTable);
        }
        free(id);
        free(numeroMatricula);
        return newAluno;
    }

    if(line == 0){
        fprintf(alunosTable,"%s,%s,%s,%s\n","ID","MATRICULA","SENHA","NOME");
        newAluno->id = 0;
    }

    newAluno->id++;

    fprintf(alunosTable,"%i,%s,%s,%s,\n",newAluno->id,newAluno->numMatricula,newAluno->senha,newAluno->nome);

    if(alunosTable) {
        fclose(alunosTable);
    }

    free(id);
    free(numeroMatricula);

    return newAluno;

}

ArrayAlunos *getAllAlunosFromFile(){
    FILE *alunosTable;
    Aluno *foundedAluno = calloc(1, sizeof(Aluno));
    int ch, property = Id, line = 0, currentSize = 0;
    int sizeId = 1, sizeMatricula = SizeNumeroMatricula, sizePassword = SizeSenha, sizeName = SizeNome;
    char *id = calloc(sizeId,sizeof(char));

    ArrayAlunos *alunos = calloc(1, sizeof(ArrayAlunos));
    alunos->alunos = calloc(1, sizeof(Aluno));
    alunos->used = 0;
    alunos->size = 1;

    foundedAluno->numMatricula = calloc(sizeMatricula, sizeof(char));
    foundedAluno->senha = calloc(sizePassword, sizeof(char));
    foundedAluno->nome = calloc(sizeName, sizeof(char));

    if((alunosTable = fopen("./tabelas/Alunos.txt", "a+")) == NULL) {
        free(id);
        return foundedAluno;
    }

    while(EOF!=(ch=fgetc(alunosTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == NumeroMatricula){
                    foundedAluno->numMatricula[currentSize++] = (char)ch;
                    if(currentSize == sizeMatricula){
                        sizeMatricula *= 2;
                        foundedAluno->numMatricula = realloc(foundedAluno->numMatricula, sizeMatricula * sizeof(char));
                    }
                }
                else if(property == Senha){
                    foundedAluno->senha[currentSize++] = (char)ch;
                    if(currentSize == sizePassword){
                        sizePassword *= 2;
                        foundedAluno->senha = realloc(foundedAluno->senha, sizePassword * sizeof(char));
                    }
                }
                else if(property == Nome){
                    foundedAluno->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAluno->nome = realloc(foundedAluno->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == Id){
                    id[currentSize++] = (char)ch;
                    foundedAluno->id = atoi(id);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        id = realloc(id, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = Id;
            currentSize = 0;

            if(line > 1){
                if (alunos->used == alunos->size) {
                    alunos->size += 1;
                    alunos->alunos = realloc(alunos->alunos, alunos->size * sizeof(Aluno));
                }

                alunos->alunos[alunos->used++] = *foundedAluno;

            }

            sizeId = 1;
            sizeMatricula = SizeNumeroMatricula;
            sizePassword = SizeSenha;
            sizeName = SizeNome;
            foundedAluno->id = 0;
            foundedAluno->numMatricula = calloc(sizeMatricula, sizeof(char));
            foundedAluno->senha = calloc(sizePassword, sizeof(char));
            foundedAluno->nome = calloc(sizeName, sizeof(char));

        }
    }

    if(line == 0){
        fprintf(alunosTable,"%s,%s,%s,%s\n","ID","MATRICULA","SENHA","NOME");
    }

    if(alunosTable) {
        fclose(alunosTable);
    }

    free(id);

    return alunos;
}

ArrayAlunos *updateAlunosOnFile(ArrayAlunos *newArrayAlunos){
    FILE *alunosTable;
    int aux;

    if((alunosTable = fopen("./tabelas/Alunos.txt", "w+")) == NULL) {
        return newArrayAlunos;
    }

    fprintf(alunosTable,"%s,%s,%s,%s\n","ID","MATRICULA","SENHA","NOME");

    for(aux = 0; aux < newArrayAlunos->size; aux++){
        fprintf(alunosTable,"%i,%s,%s,%s,\n",newArrayAlunos->alunos[aux].id, newArrayAlunos->alunos[aux].numMatricula, newArrayAlunos->alunos[aux].senha, newArrayAlunos->alunos[aux].nome);
    }

    if(alunosTable) {
        fclose(alunosTable);
    }

    return newArrayAlunos;

}

ArrayMaterias *getMateriasFromFile(int userId){
    FILE *materiasTable;
    Materia *foundedMateria = calloc(1, sizeof(Materia));
    int ch, property = IdMateria, line = 0, currentSize = 0;
    int sizeId = 1, sizeName = SizeNome;
    char *idMateria = calloc(sizeId,sizeof(char));
    char *idUsuario = calloc(sizeId,sizeof(char));

    ArrayMaterias *materias = calloc(1, sizeof(ArrayMaterias));
    materias->materias = calloc(1, sizeof(Materia));
    materias->used = 0;
    materias->size = 1;

    foundedMateria->nome = calloc(sizeName, sizeof(char));

    if((materiasTable = fopen("./tabelas/Materias.txt", "a+")) == NULL) {
        free(idMateria);
        free(idUsuario);
        materias->size = 0;
        return materias;
    }

    while(EOF!=(ch=fgetc(materiasTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdUsuario){
                  idUsuario[currentSize++] = (char)ch;
                    foundedMateria->userId = atoi(idUsuario);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idUsuario = realloc(idUsuario, sizeId * sizeof(char));
                    }
                }
                else if(property == NomeMateria){
                    foundedMateria->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedMateria->nome = realloc(foundedMateria->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == IdMateria){
                    idMateria[currentSize++] = (char)ch;
                    foundedMateria->id = atoi(idMateria);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idMateria = realloc(idMateria, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdMateria;
            currentSize = 0;
            if(userId == foundedMateria->userId){
                if (materias->used == materias->size) {
                    materias->size += 1;
                    materias->materias = realloc(materias->materias, materias->size * sizeof(Materia));
                }

                materias->materias[materias->used++] = *foundedMateria;
            }
            sizeId = 1;
            sizeName = SizeNome;
            foundedMateria->id = 0;
            foundedMateria->userId = 0;
            foundedMateria->nome = calloc(sizeName, sizeof(char));
        }
    }

    if(line == 0){
        fprintf(materiasTable,"%s,%s,%s\n","ID","ID USUARIO","NOME");
        materias->size = 0;
    }

    if(materias->used == 0){
        materias->size = 0;
    }

    if(materiasTable) {
        fclose(materiasTable);
    }

    free(idMateria);
    free(idUsuario);

    return materias;
}

Materia *postMateriaOnFile(char *nome, int userId){
    FILE *materiasTable;
    Materia *newMateria = calloc(1, sizeof(Materia));
    int ch, property = IdMateria, line = 0, currentSize = 0;
    int sizeId = 1,sizeUserId = 1, sizeName = SizeNome;
    char *id = calloc(sizeId,sizeof(char)), *foundedUserId = calloc(sizeId,sizeof(char)), *nomeMateria = calloc(sizeName, sizeof(char));

    newMateria->nome = calloc(strlen(nome), sizeof(char));

    strcat(newMateria->nome, nome);
    newMateria->userId = userId;

    if((materiasTable = fopen("./tabelas/Materias.txt", "a+")) == NULL) {
        free(id);
        free(nomeMateria);
        return newMateria;
    }

    while(EOF!=(ch=fgetc(materiasTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdMateria){
                    id[currentSize++] = (char)ch;
                    newMateria->id = atoi(id);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        id = realloc(id, sizeId * sizeof(char));
                    }
                }
                else if(property == IdUsuario){
                    foundedUserId[currentSize++] = (char)ch;
                    if(currentSize == sizeUserId){
                        sizeUserId *= 2;
                        foundedUserId = realloc(foundedUserId, sizeUserId * sizeof(char));
                    }
                }
                else if(property == NomeMateria){
                    nomeMateria[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        nomeMateria = realloc(nomeMateria, sizeName * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdMateria;
            currentSize = 0;
            sizeId = 1;
            sizeUserId =1;
            sizeName = SizeNome;
            if((strcmp(newMateria->nome, nomeMateria) == 0)&&(userId == atoi(foundedUserId))){
                newMateria->nome = "";
                break;
            }
            nomeMateria = calloc(sizeName, sizeof(char));
            id = calloc(sizeId,sizeof(char));
        }
    }

    if(newMateria->nome == ""){
        if(materiasTable) {
            fclose(materiasTable);
        }
        free(id);
        free(nomeMateria);
        return newMateria;
    }

    if(line == 0){
        fprintf(materiasTable,"%s,%s,%s\n","ID","ID USUARIO","NOME");
    }

    newMateria->id++;

    fprintf(materiasTable,"%i,%i,%s,\n",newMateria->id,newMateria->userId,newMateria->nome);

    if(materiasTable) {
        fclose(materiasTable);
    }

    free(id);
    free(nomeMateria);

    return newMateria;

}

ArrayMaterias *getAllMateriasFromFile(){
    FILE *materiasTable;
    Materia *foundedMateria = calloc(1, sizeof(Materia));
    int ch, property = IdMateria, line = 0, currentSize = 0;
    int sizeId = 1, sizeName = SizeNome;
    char *idMateria = calloc(sizeId,sizeof(char));
    char *idUsuario = calloc(sizeId,sizeof(char));

    ArrayMaterias *materias = calloc(1, sizeof(ArrayMaterias));
    materias->materias = calloc(1, sizeof(Materia));
    materias->used = 0;
    materias->size = 1;

    foundedMateria->nome = calloc(sizeName, sizeof(char));

    if((materiasTable = fopen("./tabelas/Materias.txt", "a+")) == NULL) {
        free(idMateria);
        free(idUsuario);
        materias->size = 0;
        return materias;
    }

    while(EOF!=(ch=fgetc(materiasTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdUsuario){
                  idUsuario[currentSize++] = (char)ch;
                    foundedMateria->userId = atoi(idUsuario);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idUsuario = realloc(idUsuario, sizeId * sizeof(char));
                    }
                }
                else if(property == NomeMateria){
                    foundedMateria->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedMateria->nome = realloc(foundedMateria->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == IdMateria){
                    idMateria[currentSize++] = (char)ch;
                    foundedMateria->id = atoi(idMateria);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idMateria = realloc(idMateria, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdMateria;
            currentSize = 0;
            if(line > 1){
                if (materias->used == materias->size) {
                    materias->size += 1;
                    materias->materias = realloc(materias->materias, materias->size * sizeof(Materia));
                }

                materias->materias[materias->used++] = *foundedMateria;
            }
            sizeId = 1;
            sizeName = SizeNome;
            foundedMateria->id = 0;
            foundedMateria->userId = 0;
            foundedMateria->nome = calloc(sizeName, sizeof(char));
        }
    }

    if(line == 0){
        fprintf(materiasTable,"%s,%s,%s\n","ID","ID USUARIO","NOME");
        materias->size = 0;
    }

    if(materias->used == 0){
        materias->size = 0;
    }

    if(materiasTable) {
        fclose(materiasTable);
    }

    free(idMateria);
    free(idUsuario);

    return materias;
}

ArrayMaterias *updateMateriasOnFile(ArrayMaterias *newArrayMaterias ){
    FILE *materiasTable;
    int aux;

    if((materiasTable = fopen("./tabelas/Materias.txt", "w+")) == NULL) {
        return newArrayMaterias;
    }

    fprintf(materiasTable,"%s,%s,%s\n","ID","ID USUARIO","NOME");

    for(aux = 0; aux < newArrayMaterias->size; aux++){
        fprintf(materiasTable,"%i,%i,%s,\n", newArrayMaterias->materias[aux].id, newArrayMaterias->materias[aux].userId, newArrayMaterias->materias[aux].nome);
    }

    if(materiasTable) {
        fclose(materiasTable);
    }

    return newArrayMaterias;

}

ArrayAtividades *getAtividadesFromFile(int userId, int materiaId){
    FILE *atividadesTable;
    Atividade *foundedAtividade = calloc(1, sizeof(Atividade));
    int ch, property = IdAtividade, line = 0, currentSize = 0;
    int sizeId = 1, sizeName = SizeNome, sizeDescricao = SizeDescricao, sizeValor = SizeValor, sizeData = SizeData, sizeMateria = SizeNome;
    char *idAtividade = calloc(sizeId,sizeof(char));
    char *idUsuario = calloc(sizeId,sizeof(char));
    char *idMateria = calloc(sizeId,sizeof(char));
    char *statusAtividade = calloc(sizeId,sizeof(char));

    ArrayAtividades *atividades = calloc(1, sizeof(ArrayAtividades));
    atividades->atividades = calloc(1, sizeof(Atividade));
    atividades->used = 0;
    atividades->size = 1;

    foundedAtividade->nome = calloc(sizeName, sizeof(char));
    foundedAtividade->descricao = calloc(sizeDescricao, sizeof(char));
    foundedAtividade->valor = calloc(sizeValor, sizeof(char));
    foundedAtividade->data = calloc(sizeData, sizeof(char));
    foundedAtividade->materia = calloc(sizeName, sizeof(char));

    if((atividadesTable = fopen("./tabelas/Atividades.txt", "a+")) == NULL) {
        free(idAtividade);
        free(idUsuario);
        free(idMateria);
        free(statusAtividade);
        atividades->size = 0;
        return atividades;
    }
    while(EOF!=(ch=fgetc(atividadesTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdUsuarioAtividade){
                  idUsuario[currentSize++] = (char)ch;
                    foundedAtividade->userId = atoi(idUsuario);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idUsuario = realloc(idUsuario, sizeId * sizeof(char));
                    }
                }
                else if(property == IdMateriaAtividade){
                  idMateria[currentSize++] = (char)ch;
                    foundedAtividade->materiaId = atoi(idMateria);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idMateria = realloc(idMateria, sizeId * sizeof(char));
                    }
                }
                else if(property == NomeAtividade){
                    foundedAtividade->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAtividade->nome = realloc(foundedAtividade->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == DescricaoAtividade){
                    foundedAtividade->descricao[currentSize++] = (char)ch;
                    if(currentSize == sizeDescricao){
                        sizeDescricao *= 2;
                        foundedAtividade->descricao = realloc(foundedAtividade->descricao, sizeDescricao * sizeof(char));
                    }
                }
                else if(property == ValorAtividade){
                    foundedAtividade->valor[currentSize++] = (char)ch;
                    if(currentSize == sizeValor){
                        sizeValor *= 2;
                        foundedAtividade->valor = realloc(foundedAtividade->valor, sizeValor * sizeof(char));
                    }
                }
                else if(property == DataAtividade){
                    foundedAtividade->data[currentSize++] = (char)ch;
                    if(currentSize == sizeData){
                        sizeData *= 2;
                        foundedAtividade->data = realloc(foundedAtividade->data, sizeData * sizeof(char));
                    }
                }
                else if(property == MatertiaAtividade){
                    foundedAtividade->materia[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAtividade->materia = realloc(foundedAtividade->materia, sizeName * sizeof(char));
                    }
                }
                else if(property == StatusAtividade){
                    statusAtividade[currentSize++] = (char)ch;
                    foundedAtividade->status = atoi(statusAtividade);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        statusAtividade = realloc(statusAtividade, sizeId * sizeof(char));
                    }
                }
                else if(property == IdAtividade){
                    idAtividade[currentSize++] = (char)ch;
                    foundedAtividade->id = atoi(idAtividade);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idAtividade = realloc(idAtividade, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdAtividade;
            currentSize = 0;
            if(materiaId == FALSE){
                if(userId == foundedAtividade->userId){
                    if (atividades->used == atividades->size) {
                        atividades->size += 1;
                        atividades->atividades = realloc(atividades->atividades, atividades->size * sizeof(Atividade));
                    }

                    atividades->atividades[atividades->used++] = *foundedAtividade;
                }
            }
            else{
                if((userId == foundedAtividade->userId)&&(materiaId == foundedAtividade->materiaId)){
                    if (atividades->used == atividades->size) {
                        atividades->size += 1;
                        atividades->atividades = realloc(atividades->atividades, atividades->size * sizeof(Atividade));
                    }

                    atividades->atividades[atividades->used++] = *foundedAtividade;
                }
            }
            sizeId = 1;
            sizeName = SizeNome;
            sizeDescricao = SizeDescricao;
            sizeValor = SizeValor;
            sizeData = SizeData;
            sizeMateria = SizeNome;
            foundedAtividade->id = 0;
            foundedAtividade->userId = 0;
            foundedAtividade->materiaId = 0;
            foundedAtividade->nome = calloc(sizeName, sizeof(char));
            foundedAtividade->descricao = calloc(sizeDescricao, sizeof(char));
            foundedAtividade->valor = calloc(sizeValor, sizeof(char));
            foundedAtividade->data = calloc(sizeData, sizeof(char));
            foundedAtividade->materia = calloc(sizeMateria, sizeof(char));
            foundedAtividade->status = 0;
        }
    }

    if(line == 0){
        fprintf(atividadesTable,"%s,%s,%s,%s,%s,%s,%s,%s,%s\n","ID","ID USUARIO","ID MATERIA","NOME","DESCRICAO","VALOR","DATA","MATERIA","STATUS");
        atividades->size = 0;
    }

    if(atividades->used == 0){
        atividades->size = 0;
    }

    if(atividadesTable) {
        fclose(atividadesTable);
    }

    free(IdAtividade);
    free(idUsuario);
    free(idMateria);
    free(statusAtividade);

    return atividades;
}

Atividade *postAtividadeOnFile(char *nome, char *descricao, char *valor, char *data, char *materia, int userId, int materiaId){
    FILE *atividadesTable;
    Atividade *newAtividade = calloc(1, sizeof(Atividade));
    int ch, property = IdAtividade, line = 0, currentSize = 0;
    int sizeId = 1;
    char *id = calloc(sizeId,sizeof(char));

    newAtividade->nome = calloc(strlen(nome), sizeof(char));
    newAtividade->descricao = calloc(strlen(descricao), sizeof(char));
    newAtividade->valor = calloc(strlen(valor), sizeof(char));
    newAtividade->data = calloc(strlen(data), sizeof(char));
    newAtividade->materia = calloc(strlen(materia), sizeof(char));

    strcat(newAtividade->nome, nome);
    strcat(newAtividade->descricao, descricao);
    strcat(newAtividade->valor, valor);
    strcat(newAtividade->data, data);
    strcat(newAtividade->materia, materia);
    newAtividade->userId = userId;
    newAtividade->materiaId = materiaId;
    newAtividade->status = 0;

    if((atividadesTable = fopen("./tabelas/Atividades.txt", "a+")) == NULL) {
        free(id);
        return newAtividade;
    }

    while(EOF!=(ch=fgetc(atividadesTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdAtividade){
                    id[currentSize++] = (char)ch;
                    newAtividade->id = atoi(id);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        id = realloc(id, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdAtividade;
            currentSize = 0;
            sizeId = 1;
            id = calloc(sizeId,sizeof(char));
        }
    }

    if(line == 0){
        fprintf(atividadesTable,"%s,%s,%s,%s,%s,%s,%s,%s,%s\n","ID","ID USUARIO","ID MATERIA","NOME","DESCRICAO","VALOR","DATA","MATERIA","STATUS");
    }

    newAtividade->id++;

    fprintf(atividadesTable,"%i,%i,%i,%s,%s,%s,%s,%s,%i,\n",newAtividade->id,newAtividade->userId,newAtividade->materiaId, newAtividade->nome, newAtividade->descricao, newAtividade->valor, newAtividade->data, newAtividade->materia, newAtividade->status);

    if(atividadesTable) {
        fclose(atividadesTable);
    }

    free(id);

    return newAtividade;

}

ArrayAtividades *getAllAtividadesFromFile(){
    FILE *atividadesTable;
    Atividade *foundedAtividade = calloc(1, sizeof(Atividade));
    int ch, property = IdAtividade, line = 0, currentSize = 0;
    int sizeId = 1, sizeName = SizeNome, sizeDescricao = SizeDescricao, sizeValor = SizeValor, sizeData = SizeData, sizeMateria = SizeNome;
    char *idAtividade = calloc(sizeId,sizeof(char));
    char *idUsuario = calloc(sizeId,sizeof(char));
    char *idMateria = calloc(sizeId,sizeof(char));
    char *statusAtividade = calloc(sizeId,sizeof(char));

    ArrayAtividades *atividades = calloc(1, sizeof(ArrayAtividades));
    atividades->atividades = calloc(1, sizeof(Atividade));
    atividades->used = 0;
    atividades->size = 1;

    foundedAtividade->nome = calloc(sizeName, sizeof(char));
    foundedAtividade->descricao = calloc(sizeDescricao, sizeof(char));
    foundedAtividade->valor = calloc(sizeValor, sizeof(char));
    foundedAtividade->data = calloc(sizeData, sizeof(char));
    foundedAtividade->materia = calloc(sizeName, sizeof(char));

    if((atividadesTable = fopen("./tabelas/Atividades.txt", "a+")) == NULL) {
        free(idAtividade);
        free(idUsuario);
        free(idMateria);
        free(statusAtividade);
        atividades->size = 0;
        return atividades;
    }
    while(EOF!=(ch=fgetc(atividadesTable))) {
        if(line > 0){
            if(ch == ','){
                property++;
                currentSize = 0;
            }
            else{
                if(property == IdUsuarioAtividade){
                  idUsuario[currentSize++] = (char)ch;
                    foundedAtividade->userId = atoi(idUsuario);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idUsuario = realloc(idUsuario, sizeId * sizeof(char));
                    }
                }
                else if(property == IdMateriaAtividade){
                  idMateria[currentSize++] = (char)ch;
                    foundedAtividade->materiaId = atoi(idMateria);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idMateria = realloc(idMateria, sizeId * sizeof(char));
                    }
                }
                else if(property == NomeAtividade){
                    foundedAtividade->nome[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAtividade->nome = realloc(foundedAtividade->nome, sizeName * sizeof(char));
                    }
                }
                else if(property == DescricaoAtividade){
                    foundedAtividade->descricao[currentSize++] = (char)ch;
                    if(currentSize == sizeDescricao){
                        sizeDescricao *= 2;
                        foundedAtividade->descricao = realloc(foundedAtividade->descricao, sizeDescricao * sizeof(char));
                    }
                }
                else if(property == ValorAtividade){
                    foundedAtividade->valor[currentSize++] = (char)ch;
                    if(currentSize == sizeValor){
                        sizeValor *= 2;
                        foundedAtividade->valor = realloc(foundedAtividade->valor, sizeValor * sizeof(char));
                    }
                }
                else if(property == DataAtividade){
                    foundedAtividade->data[currentSize++] = (char)ch;
                    if(currentSize == sizeData){
                        sizeData *= 2;
                        foundedAtividade->data = realloc(foundedAtividade->data, sizeData * sizeof(char));
                    }
                }
                else if(property == MatertiaAtividade){
                    foundedAtividade->materia[currentSize++] = (char)ch;
                    if(currentSize == sizeName){
                        sizeName *= 2;
                        foundedAtividade->materia = realloc(foundedAtividade->materia, sizeName * sizeof(char));
                    }
                }
                else if(property == StatusAtividade){
                    statusAtividade[currentSize++] = (char)ch;
                    foundedAtividade->status = atoi(statusAtividade);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        statusAtividade = realloc(statusAtividade, sizeId * sizeof(char));
                    }
                }
                else if(property == IdAtividade){
                    idAtividade[currentSize++] = (char)ch;
                    foundedAtividade->id = atoi(idAtividade);
                    if(currentSize == sizeId){
                        sizeId *= 2;
                        idAtividade = realloc(idAtividade, sizeId * sizeof(char));
                    }
                }
            }
        }
        if(ch == '\n'){
            line++;
            property = IdAtividade;
            currentSize = 0;
            if(line > 1){
                if (atividades->used == atividades->size) {
                    atividades->size += 1;
                    atividades->atividades = realloc(atividades->atividades, atividades->size * sizeof(Atividade));
                }

                atividades->atividades[atividades->used++] = *foundedAtividade;
            }
            sizeId = 1;
            sizeName = SizeNome;
            sizeDescricao = SizeDescricao;
            sizeValor = SizeValor;
            sizeData = SizeData;
            sizeMateria = SizeNome;
            foundedAtividade->id = 0;
            foundedAtividade->userId = 0;
            foundedAtividade->materiaId = 0;
            foundedAtividade->nome = calloc(sizeName, sizeof(char));
            foundedAtividade->descricao = calloc(sizeDescricao, sizeof(char));
            foundedAtividade->valor = calloc(sizeValor, sizeof(char));
            foundedAtividade->data = calloc(sizeData, sizeof(char));
            foundedAtividade->materia = calloc(sizeMateria, sizeof(char));
            foundedAtividade->status = 0;
        }
    }

    if(line == 0){
        fprintf(atividadesTable,"%s,%s,%s,%s,%s,%s,%s,%s,%s\n","ID","ID USUARIO","ID MATERIA","NOME","DESCRICAO","VALOR","DATA","MATERIA","STATUS");
        atividades->size = 0;
    }

    if(atividades->used == 0){
        atividades->size = 0;
    }

    if(atividadesTable) {
        fclose(atividadesTable);
    }

    free(IdAtividade);
    free(idUsuario);
    free(idMateria);
    free(statusAtividade);

    return atividades;
}

ArrayAtividades *updateAtividadesOnFile(ArrayAtividades *newArrayAtividades){
    FILE *atividadesTable;
    int aux;

    if((atividadesTable = fopen("./tabelas/Atividades.txt", "w+")) == NULL) {
        return newArrayAtividades;
    }

    fprintf(atividadesTable,"%s,%s,%s,%s,%s,%s,%s,%s,%s\n","ID","ID USUARIO","ID MATERIA","NOME","DESCRICAO","VALOR","DATA","MATERIA","STATUS");

    for(aux = 0; aux < newArrayAtividades->size; aux++){
        fprintf(atividadesTable,"%i,%i,%i,%s,%s,%s,%s,%s,%i,\n",newArrayAtividades->atividades[aux].id,newArrayAtividades->atividades[aux].userId,newArrayAtividades->atividades[aux].materiaId, newArrayAtividades->atividades[aux].nome, newArrayAtividades->atividades[aux].descricao, newArrayAtividades->atividades[aux].valor, newArrayAtividades->atividades[aux].data, newArrayAtividades->atividades[aux].materia, newArrayAtividades->atividades[aux].status);
    }

    if(atividadesTable) {
        fclose(atividadesTable);
    }

    return newArrayAtividades;

}

#endif // FILE_H_INCLUDED
